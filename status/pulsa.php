<?php
session_start();
require '../config.php';
require '../lib/session_user.php';
require '../lib/database.php';
require '../lib/header.php';
?>

<div class="app-content content ">
  <div class="content-overlay"></div>
  <div class="header-navbar-shadow"></div>
  <div class="content-wrapper">
    <div class="content-header row">
    </div>
    <div class="content-body">
      <!--Title-->
      <title>Status Transaksi PPOB</title>
      <meta name="description" content="Platform Layanan Digital All in One, Berkualitas, Cepat & Aman. Menyediakan Produk & Layanan Pemasaran Sosial Media, Payment Point Online Bank, Layanan Pembayaran Elektronik, Optimalisasi Toko Online, Voucher Game dan Produk Digital."/>

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <h4 class="m-t-0 text-uppercase text-center header-title"><i class="fa fa-history text-primary"></i> Status Transaksi PPOB</h4><hr>

              <div class="table-responsive">
                <table class="table table-striped table-bordered nowrap m-0">
                  <thead>
                    <tr>
                      <th>Order ID</th>
                      <th>Layanan</th>
                      <th>No Tujuan</th>
                      <th>status</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
// start paging config
                    if (isset($_GET['cari'])) {
                      $cari_oid = $conn->real_escape_string(filter($_GET['cari']));

    $cek_pesanan = "SELECT * FROM pembelian_pulsa WHERE target LIKE '%$cari_oid%' ORDER BY id DESC"; // edit
  } else {
    $cek_pesanan = "SELECT * FROM pembelian_pulsa ORDER BY id DESC"; // edit
  }
  if (isset($_GET['cari#status  '])) {
    $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
$records_per_page = $cari_urut; // edit
} else {
    $records_per_page = 10; // edit
  }

  $starting_position = 0;

  if(isset($_GET["halaman"])) {
    $starting_position = ($conn->real_escape_string(filter($_GET["halaman"]))-1) * $records_per_page;
  }
  $new_query = $cek_pesanan." LIMIT $starting_position, $records_per_page";
  $new_query = $conn->query($new_query);
// end paging config

  while ($data_pesanan = $new_query->fetch_assoc()) {
    if ($data_pesanan['status'] == "Pending") {
      $label = "warning";
    } else if ($data_pesanan['status'] == "Partial") {
      $label = "danger";
    } else if ($data_pesanan['status'] == "Error") {
      $label = "danger";    
    } else if ($data_pesanan['status'] == "Processing") {
      $label = "primary";    
    } else if ($data_pesanan['status'] == "Success") {
      $label = "success";    
    }
    if ($data_pesanan['refund'] == "0") {
      $icon2 = "times-circle";
      $label2 = "danger"; 
    } else if ($data_pesanan['refund'] == "1") {
      $icon2 = "check";
      $label2 = "success";
    }

    ?>
    <tr>
      <td><?php echo $data_pesanan['oid']; ?></td>

      <td><?php echo $data_pesanan['layanan']; ?></td>
      <td>
        <?php 
        $noTlp = $data_pesanan['target'];
        echo substr($noTlp, 0, -3) . 'XXX';

        ?>
      </td>

      <td><span class="badge badge-<?php echo $label; ?>"><?php echo $data_pesanan['status']; ?></span></td>
    </tr>   
  <?php } ?>
</tbody>
</table>
</div>

</div>
</div>
</div>
</div>
<!-- Begin Page Content -->
<div class="container-fluid">


  <!-- Content Row -->
  <div class="row">
   <div class="col-xl-12 col-lg-7">
    <div class="card shadow mb-4">

      <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-sync fa-spin"></i> STATUS TRANSAKSI</h6>

      </div>
      <!-- Card Body -->
      <div class="card-body">

       <div id="stat"></div>

       <div class="row">
        <div class="col-sm-4">
          <p class="font-15 max-width-900"> 
            <b>
             <i class="fa fa-info-circle"></i>   Keterangan Status 
           </b>
         </p>
         <table class="table" style="margin-bottom: 15px;">
          <tbody>
            <tr>
              <td style="width: 40px;">
                <span class="badge badge-success">Success</span>
              </td>
              <td>
                Pengisian Berhasil
              </td>
            </tr>
            <tr>
              <td>

                <span class="badge badge-warning">Pending</span>

              </td>
              <td>
                Sedang kami proses.
              </td>
            </tr>
            <tr>

              <tr>
                <td>
                  <span class="badge" style="width: 34px;">
                    <span class="badge badge-danger">Error</span>
                  </span>
                </td>
                <td>
                  Refund, Nomer habis masa aktif atau Nomer salah.
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="col-sm-8">
          <p>
            <b>
             <i class="fa fa-info-circle"></i>  Pulsa Telpon, Token, atau Voucher Belum Masuk?
           </b>
         </p>
         <ul>
          <li>
            Terlebih dahulu cek pulsa Anda. Kadang pulsa sudah masuk mendahului report / notifikasi melalui SMS.
          </li>
          <li>
            Khusus Token PLN, Token akan otomatis kami kirimkan ke HP anda atau bisa langsung anda cetak di halaman informasi order.
          </li>
        </ul>

      </ul>
    </div>
  </div>
</div>

</div>
</div>
<?php
require '../lib/footer.php';
?>

