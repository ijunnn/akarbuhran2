<?php
session_start();
require '../config.php';
require '../lib/session_user.php';
require '../lib/database.php';
require '../lib/header.php';
?>

<div class="app-content content ">
  <div class="content-overlay"></div>
  <div class="header-navbar-shadow"></div>
  <div class="content-wrapper">
    <div class="content-header row">
    </div>
    <div class="content-body">
      <!--Title-->
      <title>Monitoring Layanan</title>
      <meta name="description" content="Platform Layanan Digital All in One, Berkualitas, Cepat & Aman. Menyediakan Produk & Layanan Pemasaran Sosial Media, Payment Point Online Bank, Layanan Pembayaran Elektronik, Optimalisasi Toko Online, Voucher Game dan Produk Digital."/>

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <h4 class="m-t-0 text-uppercase text-center header-title"><i class="fa fa-history text-primary"></i> Monitoring Layanan</h4><hr>

              <div class="table-responsive">
                <table class="table table-striped table-bordered nowrap m-0">
                  <thead>
                    <tr>
                      <th>Order ID</th>
                      <th>Layanan</th>
                      <th>Waktu Pesan</th>
                      <th>Jumlah</th>
                      <th>status</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
// start paging config
                    if (isset($_GET['cari'])) {
                      $cari_oid = $conn->real_escape_string(filter($_GET['cari']));

    $cek_pesanan = "SELECT * FROM pembelian_sosmed WHERE target LIKE '%$cari_oid%' ORDER BY id DESC"; // edit
  } else {
    $cek_pesanan = "SELECT * FROM pembelian_sosmed ORDER BY id DESC"; // edit
  }
  if (isset($_GET['cari#status  '])) {
    $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
$records_per_page = $cari_urut; // edit
} else {
    $records_per_page = 10; // edit
  }

  $starting_position = 0;

  if(isset($_GET["halaman"])) {
    $starting_position = ($conn->real_escape_string(filter($_GET["halaman"]))-1) * $records_per_page;
  }
  $new_query = $cek_pesanan." LIMIT $starting_position, $records_per_page";
  $new_query = $conn->query($new_query);
// end paging config

  while ($data_pesanan = $new_query->fetch_assoc()) {
    if ($data_pesanan['status'] == "Pending") {
      $label = "warning";
    } else if ($data_pesanan['status'] == "Partial") {
      $label = "danger";
    } else if ($data_pesanan['status'] == "Error") {
      $label = "danger";    
    } else if ($data_pesanan['status'] == "Processing") {
      $label = "primary";    
    } else if ($data_pesanan['status'] == "Success") {
      $label = "success";    
    }
    if ($data_pesanan['refund'] == "0") {
      $icon2 = "times-circle";
      $label2 = "danger"; 
    } else if ($data_pesanan['refund'] == "1") {
      $icon2 = "check";
      $label2 = "success";
    }

    ?>
    <tr>
      <td><?php echo $data_pesanan['oid']; ?></td>

      <td><?php echo $data_pesanan['layanan']; ?></td>
      <td><?php echo tanggal_indo($data_pesanan['date']); ?>, <?php echo $data_pesanan['time']; ?></td>
      <td><?php echo $data_pesanan['jumlah']; ?></td>

      <td><span class="badge badge-<?php echo $label; ?>"><?php echo $data_pesanan['status']; ?></span></td>
    </tr>   
  <?php } ?>
</tbody>
</table>
</div>

</div>
</div>
</div>
</div>



</div>
</div>
<?php
require '../lib/footer.php';
?>

