<?php
session_start();
require '../config.php';
require '../lib/session_user.php';
require '../lib/session_login.php';
require("../lib/header.php");
?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="m-t-0 "><i class="fa fa-area-chart fa-fw"></i> Grafik Pesanan 7 Hari Terakhir</h4>
                         <div id="line-chart" style="height: 300px;"></div>
<script type="text/javascript">
$(function(){
  new Morris.Area({
    element: 'line-chart',
    data: [
<?php
$list_tanggal = array();
for ($i = 6; $i > -1; $i--) {
    $list_tanggal[] = date('Y-m-d', strtotime('-'.$i.' days'));
}

for ($i = 0; $i < count($list_tanggal); $i++) {
    $get_order_sosmed = $conn->query("SELECT * FROM pembelian_sosmed WHERE date ='".$list_tanggal[$i]."' and user = '$sess_username'");
    $get_order_pulsa = $conn->query("SELECT * FROM pembelian_pulsa WHERE date = '".$list_tanggal[$i]."' ");
    print("{ y: '".tanggal_indo($list_tanggal[$i])."', a: ".mysqli_num_rows($get_order_sosmed).", b: ".mysqli_num_rows($get_order_pulsa)." }, ");

}
?>
    ],
        xkey: 'y',
        ykeys: ['a','b'],
        labels: ['Pesanan Sosial Media'],
        lineColors: ['#525af2'],
        gridLineColor: '#000000',
        pointSize: 0,
        lineWidth: 0,
        resize: true,
        parseTime: false
    });
  });
</script>                          
                        </div>
                    </div>
                </div>
                </div>   
<?php
require ("../lib/footer.php");
?>