<?php
session_start();
require '../config.php';
require '../lib/header.php';
require '../lib/session_user.php';

$pembelian_sosmed = $conn->query("SELECT SUM(pembelian_sosmed.harga) AS total_pembelian, count(pembelian_sosmed.id) AS tcount, pembelian_sosmed.user, users.nama FROM pembelian_sosmed JOIN users ON pembelian_sosmed.user = users.username WHERE MONTH(pembelian_sosmed.date) = '".date('m')."' AND YEAR(pembelian_sosmed.date) = '".date('Y')."' GROUP BY pembelian_sosmed.user ORDER BY total_pembelian DESC LIMIT 10");

$pembelian_pulsa = $conn->query("SELECT SUM(pembelian_pulsa.harga) AS total_pembelian, count(pembelian_pulsa.id) AS tcount, pembelian_pulsa.user, users.nama FROM pembelian_pulsa JOIN users ON pembelian_pulsa.user = users.username WHERE MONTH(pembelian_pulsa.date) = '".date('m')."' AND YEAR(pembelian_pulsa.date) = '".date('Y')."' GROUP BY pembelian_pulsa.user ORDER BY total_pembelian DESC LIMIT 10");
?>
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
<!--Title-->
<title>TOP 10 NOMINAL PEMBELIAN TERBESAR <?php echo $data['short_title']; ?></title>
<meta name="description" content="Top 10 Pengguna <?php echo $data['short_title']; ?>"/>

<!--OG2-->
<meta content="Top 10 Pengguna <?php echo $data['short_title']; ?>" property="og:title"/>
<meta content="Top 10 Pengguna <?php echo $data['short_title']; ?>" property="og:description"/>
<meta content="Top 10 Pengguna <?php echo $data['short_title']; ?>"/>
<meta content="<?php echo $config['web']['url'];?>assets/images/halaman/tentang-kami.png" property="og:image"/>
<meta content="Top 10 Pengguna <?php echo $data['short_title']; ?>" property="twitter:title"/>
<meta content="Top 10 Pengguna <?php echo $data['short_title']; ?>" property="twitter:description"/>
<meta content="<?php echo $config['web']['url'];?>assets/images/halaman/tentang-kami.png" property="twitter:image"/>

<div class="row">
	<div class="col-md-12">
		<div class="card-box">
			<center>                                
				<h1 class="m-t-0 text-uppercase text-center header-title"><b>TOP 10 NOMINAL PEMBELIAN TERBESAR <?php echo $data['short_title']; ?></b></h1>
				<p>Kami mengucapkan terimakasih telah menjadi pelanggan setia <?php echo $data['short_title']; ?></p>
			</center>
			<br/>

				<div class="card">
					<div class="card-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered nowrap m-0">
								<thead>
									<tr>
										<th>No</th>
										<th>Username</th>
										<th>Jumlah</th>
										<th>Nominal</th>
									</tr>
								</thead>
								<tbody>
									<?php 
									$no = 1;
									while($data = mysqli_fetch_array($pembelian_sosmed)) {
										$total_pembelian = number_format($data['total_pembelian'],0,',','.');
										$tcount = number_format($data['tcount'],0,',','.');
										?>
										<tr>
											<td><span class="badge badge-dark"><?php echo $no++; ?></span></td>
											<td><?php echo $data['user']; ?></td>
											<td><?php echo $tcount; ?> Pesanan</td>
											<td><span class="btn btn-success btn-xs"><b> Rp <?php echo $total_pembelian; ?></span></b></td>
										</tr>

									<?php } ?>
								</tbody>
							</table>
						</div>
					</ul>
				</div>
		</div> <!-- card-box -->
	</div> <!-- /col-md-12 -->
</div> <!-- /row -->

<?php
require '../lib/footer.php';
?>