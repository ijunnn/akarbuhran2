<?php
session_start();
require '../config.php';
require '../lib/header.php';
?>
<!--Title-->
<title>Informasi Status Order</title>
<meta name="description" content="Informasi Status Order Layanan SMM Panel Indonesia dan Pulsa H2H Sistem PPOB di <?php echo $data['title']; ?>."/>

<!--OG2-->
<meta content="Informasi Status Order Produk dan Layanan <?php echo $data['short_title']; ?>" property="og:title"/>
<meta content="Informasi Status Order Layanan SMM Panel Indonesia dan Pulsa H2H Sistem PPOB di <?php echo $data['title']; ?>." property="og:description"/>
<meta content="<?php echo $data['short_title']; ?> - KETERANGAN ORDER PRODUK DAN LAYANAN <?php echo $data['short_title']; ?>" property="og:headline"/>
<meta content="<?php echo $config['web']['url'];?>assets/images/halaman/tentang-kami.png" property="og:image"/>
<meta content="Informasi Status Order Produk dan Layanan <?php echo $data['short_title']; ?>" property="twitter:title"/>
<meta content="Informasi Status Order Layanan SMM Panel Indonesia dan Pulsa H2H Sistem PPOB di <?php echo $data['title']; ?>." property="twitter:description"/>
<meta content="<?php echo $config['web']['url'];?>assets/images/halaman/tentang-kami.png" property="twitter:image"/>

<div class="row">
	<div class="col-sm-12">
		<div class="card">    
			<div class="card-body table-responsive">  
				<center>
					<h1 class="m-t-0 text-uppercase text-center header-title"><b>Informasi Status Order</b></h1></center>
				</br>
				<center>
					<span class="badge badge-success">SUCCESS</span>                               
					<p>Pesanan telah berhasil diproses.</p>
					<br>
					<span class="badge badge-info">PROCESSING</span>                               
					<p>Pesanan sedang dalam proses.</p>
					<br>
					<span class="badge badge-warning">PENDING</span>
					<p>Pesanan sedang dalam antrian di server.</p>
					<br>
					<span class="badge badge-danger">PARTIAL</span>                               
					<p>Pesanan hanya masuk sebagian. Anda hanya membayar yang masuk saja.</p>
					<br>
					<span class="badge badge-danger">ERROR</span>                               
					<p>Terjadi kesalahan sistem dan saldo akan otomatis kembali ke akun anda.</p>
					<br>
					<span class="badge badge-primary">REFILL</span>                               
					<p>Jika anda membeli layanan refill untuk auto follower dan ternyata beberapa hari followers berkurang, maka akan otomatis di refill/isi ulang gratis.</p>
					<p><font color="#000000"><b>Catatan:</b> server hanya akan mengisi ulang jika followers yang berkurang adalah followers yang dibeli dengan layanan refill.</font></p></center>             
				</div>
		 	</div>

		 	<div class="card">
		 		<div class="card-body table-responsive">
		 			<center>
		 				<h1 class="m-t-0 text-uppercase text-center header-title"><b>Contoh Target Pesanan</b></h1></center>
		 			</br>
		 				<b>Instagram</b>
		 			</p><ul>
		 				<li>Instagram Followers, Story, Live Video, Profile Visits : Username akun Instagram tanpa tanda @ // Contoh : kincaimedia</li>
		 				<li>Instagram Likes, Views, Comments, Impressions, Saves, IGTV : Link postingan akun Instagram // Contoh : https://www.instagram.com/p/CDIDw7IDVAN/</li>
		 			</ul>

		 			<b>Youtube</b>
		 			<ul>
		 				<li>Youtube Likes, Views, Shares, Comments : Link video youtube // Contoh : https://www.youtube.com/watch?v=SAkFxg75EAM</li>
		 				<li>Youtube Live Streaming, Jam Tayang/Watchtime : Link video youtube // Contoh : https://www.youtube.com/watch?v=SAkFxg75EAM</li>
		 				<li>Youtube Subscribers : Link channel youtube // Contoh : https://www.youtube.com/channel/UChqhDAG5foszPZVGeA98HbQ atau https://www.youtube.com/c/kincaimedia</li>
		 			</ul>

		 			<b>Facebook</b>
		 			<ul>
		 				<li>Facebook Page Likes, Page Followers : Link halaman atau fanspage facebook // Contoh : https://www.facebook.com/nama_akun/</li>
		 				<li>Facebook Post Likes, Commnects, Post Video : Link postingan facebook // Contoh : https://www.facebook.com/nama_akun/posts/3459400887762389 (kadang diawali dengan m.facebook.com jika link dari mobile)</li>
		 				<li>Facebook Followers, Friends : Link profile facebook // Contoh : https://www.facebook.com/nama_akunl</li>
		 				<li>Facebook Group Members : Link group facebook // Contoh : https://www.facebook.com/groups/forumadsenseindonesia</li>
		 			</ul>

		 			<b>Twitter</b>
		 			<ul>
		 				<li>Twitter Followers : Username akun twitter tanpa tanda @ // Contoh : nama_akun</li>
		 				<li>Twitter Retweet, Favorite : Link tweet atau postingan twitter // Contoh : https://twitter.com/nama_akun/status/1351742017726889984</li>
		 			</ul>

		 			<b>Tik Tok</b>
		 			<ul>
		 				<li>Tik Tok Followers : Link profile tik tok atau username tanpa tanda @ // Contoh : nama_akun</li>
		 				<li>Tik Tok Likes / Views : Link video tik tok // Contoh : https://www.tiktok.com/@nama_akun/video/6913480030462954754</li>
		 			</ul>

		 			<b>Shopee</b>
		 			<ul>
		 				<li>Shopee Followers : Username akun shopee // Contoh : nama_akun</li>
		 				<li>Shopee Product Likes : Link produk shopee // Contoh : https://shopee.co.id/M231-Kemeja-Pria-Batik-Navy.197800</li>
		 			</ul>

		 			<b>Tokopedia</b>
		 			<ul>
		 				<li>Tokopedia Followers : Username akun tokopedia atau link profile // Contoh : https://www.tokopedia.com/nama_akun</li>
		 				<li>Tokopedia Wishlist atau Favorite : Link produk tokopedia // Contoh : https://www.tokopedia.com/nama_akun/barang-premium
		 				</li>
		 			</ul>

		 			<b>Website Traffic</b>
		 			<ul>
		 				<li>Website Traffic : Link website // Contoh : https://boostnaa.com.com/halaman/produk-dan-layanan</li>
		 			</ul>
		 		</div>
		 	</div>

		</div>
	</div>

	<?php
	require '../lib/footer.php';
	?>