<?php
session_start();
require '../config.php';
require '../lib/header.php';
?>
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
<!--Title-->
<title>Daftar Harga Layanan SMM dan PPOB <?php echo $data['short_title']; ?></title>
<meta name="description" content="Berikut adalah daftar layanan jasa Sosial Media, Toko Online, Payment Point Online Bank (PPOB), dan berbagai produk virtual yang tersedia di <?php echo $data['short_title']; ?>."/>

<!--OG2-->
<meta content="Daftar Harga Layanan SMM dan PPOB <?php echo $data['short_title']; ?>" property="og:title"/>
<meta content="Berikut adalah daftar layanan jasa Sosial Media, Toko Online, Payment Point Online Bank (PPOB), dan berbagai produk virtual yang tersedia di <?php echo $data['short_title']; ?>." property="og:description"/>
<meta content="<?php echo $data['short_title']; ?> - Daftar Harga Layanan SMM dan PPOB <?php echo $data['short_title']; ?>" property="og:headline"/>
<meta content="<?php echo $config['web']['url'];?>assets/images/halaman/produk-dan-layanan.png" property="og:image"/>
<meta content="Daftar Harga Layanan SMM dan PPOB <?php echo $data['short_title']; ?>" property="twitter:title"/>
<meta content="Berikut adalah daftar layanan jasa Sosial Media, Toko Online, Payment Point Online Bank (PPOB), dan berbagai produk virtual yang tersedia di <?php echo $data['short_title']; ?>." property="twitter:description"/>
<meta content="<?php echo $config['web']['url'];?>assets/images/halaman/produk-dan-layanan.png" property="twitter:image"/>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="m-t-0 m-b-30 header-title"><i class="fa fa-tags"></i> Daftar Harga Layanan Media Sosial</h4>                            
                <form>
                    <div class="row">
                        <div class="form-group col-lg-4">
                            <label>Tampilkan Beberapa</label>
                            <select class="form-control" name="tampil">
                                <option value="10">10</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                                <option value="250">250</option>
                            </select>
                        </div>                                                             <div class="form-group col-lg-4">
                            <label>Cari Layanan</label>
                            <input type="text" class="form-control" name="search" placeholder="Cari Layanan" value="">
                        </div>
                        <div class="form-group col-lg-4">
                            <label>Submit</label>
                            <button type="submit" class="btn btn-block btn-dark">Filter</button>
                        </div>
                    </div>
                </form>

                <div class="table-responsive">
                    <table class="table table-striped table-bordered nowrap m-0">
                        <thead>
                            <tr>
                                <th>Service ID</th>
                                <th>Kategori</th>
                                <th>Layanan</th>
                                <th>Min Pesan</th>
                                <th>Max Pesan</th>
                                <th>Harga/k</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
// start paging config
                            if (isset($_GET['search'])) {
                                $search = $conn->real_escape_string(filter($_GET['search']));

$cek_layanan = "SELECT * FROM layanan_sosmed WHERE layanan LIKE '%$search%' ORDER BY id ASC"; // edit
} else {
$cek_layanan = "SELECT * FROM layanan_sosmed ORDER BY id ASC"; // edit
}
if (isset($_GET['search'])) {
    $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
$records_per_page = $cari_urut; // edit
} else {
$records_per_page = 10; // edit
}

$starting_position = 0;
if(isset($_GET["halaman"])) {
    $starting_position = ($conn->real_escape_string(filter($_GET["halaman"]))-1) * $records_per_page;
}
$new_query = $cek_layanan." LIMIT $starting_position, $records_per_page";
$new_query = $conn->query($new_query);
// end paging config
while ($data_layanan = $new_query->fetch_assoc()) {
    ?>
    <tr> 
        <td><?php echo $data_layanan['service_id']; ?></td>
        <td><?php echo $data_layanan['kategori']; ?></td>
        <td><?php echo $data_layanan['layanan']; ?></td>
        <td><?php echo $data_layanan['min']; ?></td>
        <td><?php echo $data_layanan['max']; ?></td>
        <td><?php echo $data_layanan['harga']; ?></td>
        <td><?php echo $data_layanan['status']; ?></td>
    </tr>  
<?php } ?>
</tbody>
</table>
</div>
<br>
<ul class="pagination pagination-split">
    <?php
// start paging link
    if (isset($_GET['search'])) {
        $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
    } else {
        $cari_urut =  10;
    }  
    if (isset($_GET['search'])) {
        $search = $conn->real_escape_string(filter($_GET['search']));
        $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
    } else {
        $self = $_SERVER['PHP_SELF'];
    }
    $cek_layanan = $conn->query($cek_layanan);
    $total_records = mysqli_num_rows($cek_layanan);
    echo "<li class='disabled page-item'><a class='page-link' href='#'>Total: ".$total_records."</a></li>";
    if($total_records > 0) {
        $total_pages = ceil($total_records/$records_per_page);
        $current_page = 1;
        if(isset($_GET["halaman"])) {
            $current_page = $conn->real_escape_string(filter($_GET["halaman"]));
            if ($current_page < 1) {
                $current_page = 1;
            }
        }
        if($current_page > 1) {
            $previous = $current_page-1;
            if (isset($_GET['search'])) {
                $search = $conn->real_escape_string(filter($_GET['search']));
                $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
                echo "<li class='page-item'><a class='page-link' href='".$self."?halaman=1&tampil=".$cari_urut."&search=".$search."'><<</a></li>";
                echo "<li class='page-item'><a class='page-link' href='".$self."?halaman=".$previous."&tampil=".$cari_urut."&search=".$search."'><</a></li>";
            } else {
                echo "<li class='page-item'><a class='page-link' href='".$self."?halaman=1'><<</a></li>";
                echo "<li class='page-item'><a class='page-link' href='".$self."?halaman=".$previous."'><</a></li>";
            }
        }
// limit page
        $limit_page = $current_page+3;
        $limit_show_link = $total_pages-$limit_page;
        if ($limit_show_link < 0) {
            $limit_show_link2 = $limit_show_link*2;
            $limit_link = $limit_show_link - $limit_show_link2;
            $limit_link = 3 - $limit_link;
        } else {
            $limit_link = 3;
        }
        $limit_page = $current_page+$limit_link;
// end limit page
// start page
        if ($current_page == 1) {
            $start_page = 1;
        } else if ($current_page > 1) {
            if ($current_page < 4) {
                $min_page  = $current_page-1;
            } else {
                $min_page  = 3;
            }
            $start_page = $current_page-$min_page;
        } else {
            $start_page = $current_page;
        }
// end start page
        for($i=$start_page; $i<=$limit_page; $i++) {
            if (isset($_GET['search'])) {
                $search = $conn->real_escape_string(filter($_GET['search']));
                $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
                if($i==$current_page) {
                    echo "<li class='active page-item'><a class='page-link' href='#'>".$i."</a></li>";
                } else {
                    echo "<li class='page-item'><a class='page-link' href='".$self."?halaman=".$i."&tampil=".$cari_urut."&search=".$search."'>".$i."</a></li>";
                }
            } else {
                if($i==$current_page) {
                    echo "<li class='active page-item'><a class='page-link' href='#'>".$i."</a></li>";
                } else {
                    echo "<li class='page-item'><a class='page-link' href='".$self."?halaman=".$i."'>".$i."</a></li>";
                }        
            }
        }
        if($current_page!=$total_pages) {
            $next = $current_page+1;
            if (isset($_GET['cari'])) {
                $cari_oid = $conn->real_escape_string(filter($_GET['cari']));
                $cari_status = $conn->real_escape_string(filter($_GET['status']));
                $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
                echo "<li class='page-item'><a class='page-link' href='".$self."?halaman=".$next."&tampil=".$cari_urut."&search=".$search."'>></a></li>";
                echo "<li class='page-item'><a class='page-link' href='".$self."?halaman=".$total_pages."&tampil=".$cari_urut."&search=".$search."'>>></a></li>";
            } else {
                echo "<li class='page-item'><a class='page-link' href='".$self."?halaman=".$next."'>></i></a></li>";
                echo "<li class='page-item'><a class='page-link' href='".$self."?halaman=".$total_pages."'>>></a></li>";
            }
        }
    }
// end paging link
    ?>
</ul>
</div>
</div>
</div>
</div>

<?php
require '../lib/footer.php';
?>