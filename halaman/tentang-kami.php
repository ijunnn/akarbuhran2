<?php
session_start();
require '../config.php';
require '../lib/header.php';
?>
<!--Title-->
<title>Sekilas Tentang <?php echo $data['short_title']; ?></title>
<meta name="description" content="Platform Layanan Digital All in One, Berkualitas, Cepat & Aman. Menyediakan Produk & Layanan Pemasaran Sosial Media, Payment Point Online Bank, Layanan Pembayaran Elektronik, Optimalisasi Toko Online, Voucher Game dan Produk Digital."/>

<!--OG2-->
<meta content="Sekilas Tentang <?php echo $data['short_title']; ?>" property="og:title"/>
<meta content="Platform Layanan Digital All in One, Berkualitas, Cepat & Aman. Menyediakan Produk & Layanan Pemasaran Sosial Media, Payment Point Online Bank, Layanan Pembayaran Elektronik, Optimalisasi Toko Online, Voucher Game dan Produk Digital." property="og:description"/>
<meta content="Sekilas Tentang <?php echo $data['short_title']; ?>" property="og:headline"/>
<meta content="<?php echo $config['web']['url'];?>assets/images/halaman/tentang-kami.png" property="og:image"/>
<meta content="Sekilas Tentang <?php echo $data['short_title']; ?>" property="twitter:title"/>
<meta content="Platform Layanan Digital All in One, Berkualitas, Cepat & Aman. Menyediakan Produk & Layanan Pemasaran Sosial Media, Payment Point Online Bank, Layanan Pembayaran Elektronik, Optimalisasi Toko Online, Voucher Game dan Produk Digital." property="twitter:description"/>
<meta content="<?php echo $config['web']['url'];?>assets/images/halaman/tentang-kami.png" property="twitter:image"/>

<link href="../home/css/style.css" rel="stylesheet" type="text/css"/>
<div class="row">
	<div class="col-sm-12">
		<div class="card">

			<div class="card-body table-responsive">
				<center><h1 class="m-t-0 text-uppercase text-center header-title"><b>Sekilas Tentang <?php echo $data['short_title']; ?></b></h1></center>
				<?php 
//Call Tentang Kami
				$CallPage = Show('halaman', "id = '1'");
				echo "".$CallPage['konten']."";
				?>
			</div>

			<div class="card-body table-responsive">
				<div class="container">
					<div class="section-content row">

						<div class="col-sm-6 align-left">
							<div>
								<h2 class="text-uppercase text-center header-title"><b>Mengapa Memilih Kami?</b></h2>
								Kami menyediakan layanan otomatisasi berteknologi AI, berkualitas, cepat & aman. Dengan mendaftar, Anda bisa menjadi Member, Agen atau Reseller layanan jasa otomatisasi Sosial Media, optimasi Toko Online, penyedia Pembayaran Online dan Produk Digital.<br><br>
								<?php echo $data['short_title']; ?> adalah startup terbaru generasi milenial 4.0 yang diluncurkan <a title="401XD Group" alt="401XD Group" href="http://www.401xd.com" target="_blank" rel="nofollow noopener" class="seoquake-nofollow"><b>401XD Group</b></a>, kami berkomitmen membantu Anda dalam pengembangan bisnis di internet, baik untuk perusahaan, merk/brand, bisnis lokal, public figure, maupun perorangan/pribadi.<br><br>

								<h2 class="text-uppercase text-center header-title"><b>Layanan Sosial Media Terlengkap</b></h2>
								Tersedia layanan <a title="Server Panel SMM Termurah Indonesia" alt="Panel Social Media Marketing Indonesia" target="_blank" href="../halaman/daftar-harga#SosialMedia"><b>Sosial Media Marketing</b></a> All-In-One untuk optimasi Youtube, Facebook, Instagram, Twitter, Pinterest, Tiktok dan WhatsApp. Juga terdapat layanan toko online E-Commerce untuk Shopee, Tokopedia dan Bukalapak.<br><br>

								<h3 class="text-uppercase text-center header-title"><b>Konten Panduan <?php echo $data['short_title']; ?></b></h3>
								Kami menyediakan blog panduan menggunakan fitur dan layanan, diantaranya cara mengoptimalkan sosial media, subscribe youtube, trafik website, hingga panduan transaksi jasa dan layanan di <?php echo $data['short_title']; ?> yang bisa Anda pelajari.<br><br>
							</div>
						</div>

						<div class="col-sm-6 pull-right">
							<div>
								<h2 class="text-uppercase text-center header-title"><b>Layanan Terkoneksi PPOB</b></h2>
								Tersedia berbagai Produk Digital dengan sistem PPOB untuk transaksi E-Money (Gojek, Grab, Dana, Ovo & Steam Wallet), <a title="Situs Voucher Game Murah" alt="Situs Voucher Game Murah" target="_blank" href="../halaman/daftar-harga#TopUpGames"><b>Voucher All Games</b></a>, Token PLN, <a title="Server Pulsa H2H Termurah" alt="Server Pulsa Reguler dan Transfer H2H Termurah" target="_blank" href="../halaman/daftar-harga#PulsaReguler"><b>Pulsa & Paket Internet All Operator</b></a> Prabayar & Pascabayar, Paket Telepon & SMS, hingga <a title="Situs H2H Produk Operator Global" alt="Situs Server H2H Produk Operator Global" target="_blank" href="../halaman/daftar-harga#TopUpGames"><b>Produk Operator Global</b></a>.<br><br>
								Sistem PPOB adalah metode pembayaran yang terkoneksi secara online dalam waktu cepat & aman, dengan menjalin kerjasama dengan berbagai pihak, setiap transaksi valid akan secara otomatis terkonfirmasi sehingga Anda tidak perlu melakukan konfirmasi lagi.<br><br>

								<h2 class="text-uppercase text-center header-title"><b>Jasa Exchanger & Perbankan</b></h2>
								Layanan exchange tersedia untuk pertukaran (jual/beli) dollar, cryptocurrency, dan voucher indodax. Sedangkan pada jasa perbankan hanya E-Money Changer dan BRI Brizzi. Ini tersedia untuk semua Member, Agen, atau Reseller.<br><br>

								<h3 class="text-uppercase text-center header-title"><b>Portal Edukasi Mediabisnis.co.id</b></h3>
								Kami juga berfokus mengembangan situs portal bisnis dan investasi. Dapatkan panduan Internet Marketing, Digital Marketing, Bisnis Affiliate, Cryptocurrency, Forex dan panduan Saham di <a title="Portal Edukasi Bisnis dan Investasi" alt="Portal Edukasi Bisnis dan Investasi" href="https://www.mediabisnis.co.id" target="_blank" rel="nofollow noopener" class="seoquake-nofollow"><b>Portal Edukasi Bisnis & Investasi</b>.</a><br><br>
							</div>
						</div>

					</div>
				</div>
			</div>

		</div>
	</div>
</div>

<?php
require '../lib/footer.php';
?>