<?php
session_start();
require '../config.php';
require '../lib/header.php';
?>
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
<!--Title-->
<title>Daftar Produk dan Layanan <?php echo $data['short_title']; ?></title>
<meta name="description" content="Berikut adalah daftar layanan jasa Sosial Media, Toko Online, Payment Point Online Bank (PPOB), dan berbagai produk virtual yang tersedia di <?php echo $data['title']; ?>."/>

<!--OG2-->
<meta content="Daftar Produk dan Layanan <?php echo $data['short_title']; ?>" property="og:title"/>
<meta content="Berikut adalah daftar layanan jasa Sosial Media, Toko Online, Payment Point Online Bank (PPOB), dan berbagai produk virtual yang tersedia di <?php echo $data['title']; ?>." property="og:description"/>
<meta content="<?php echo $data['short_title']; ?> - Daftar Produk dan Layanan <?php echo $data['short_title']; ?>" property="og:headline"/>
<meta content="<?php echo $config['web']['url'];?>assets/images/halaman/produk-dan-layanan.png" property="og:image"/>
<meta content="Daftar Produk dan Layanan <?php echo $data['short_title']; ?>" property="twitter:title"/>
<meta content="Berikut adalah daftar layanan jasa Sosial Media, Toko Online, Payment Point Online Bank (PPOB), dan berbagai produk virtual yang tersedia di <?php echo $data['title']; ?>." property="twitter:description"/>
<meta content="h<?php echo $config['web']['url'];?>assets/images/halaman/produk-dan-layanan.png" property="twitter:image"/>

<div class="row">
	<div class="col-md-12">
		<div class="card">

			<div class="card-body table-responsive">
				<center><h1 class="m-t-0 text-uppercase text-center header-title"><b>Daftar Produk dan Layanan <?php echo $data['short_title']; ?></b></h1></center>
				<?php 
//Call Produk dan Layanan
				$CallPage = Show('halaman', "id = '3'");
				echo "".$CallPage['konten']."";
				?>
			</div>

			<div class="card-body table-responsive">
				<!--Navigasi v1-->
				<ul class="nav nav-tabs tabs-bordered d-none">
					<li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#SosialMedia"> SOSIAL MEDIA</a></li>
					<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#AkunPremium"> AKUN PREMIUM</a></li>
					<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#TopUpEMoney"> SALDO E-MONEY</a></li>
					<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#TopUpGames"> VOUCHER GAMES</a></li>
					<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#VoucherDigital"> VOUCHER DIGITAL</a></li>
					<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#TokenPLN"> TOKEN PLN</a></li>
					<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#PaketInternet"> PAKET INTERNET</a></li>
					<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#PulsaReguler"> PULSA REGULER</a></li>
					<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#TeleponSMS"> PAKET TELEPON & SMS</a></li>
					<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#ProvideGlobal"> PROVIDER GLOBAL</a></li>
				</ul>

				<!--Navigasi v2-->
				<center>
				<div class="input-group" style="width: 270px">
					<div class="input-group-prepend">
						<span class="btn btn btn-primary" id="navigasi" ><i class="fa fa-cog fa-spin"></i> KATEGORI </span>
					</div>
					<select name="category" class="form-control" id="filter_kategori">
						<option value="SosialMedia">Jasa Sosial Media</option>
						<option value="AkunPremium">Akun Premium</option>
						<option value="TopUpEMoney">Top Up E-Money</option>
						<option value="TopUpGames">Top Up Game</option>
						<option value="VoucherDigital">Top Up Voucher</option>
						<option value="TokenPLN">Token PLN</option>
						<option value="PaketInternet">Paket Internet</option>
						<option value="PulsaReguler">Pulsa Reguler</option>
						<option value="TeleponSMS">Paket SMS & Telepon</option>
						<option value="ProvideGlobal">Provider Global</option>
					</select>
				</div>
				</center>

				<div class="tab-content">
					<div id="SosialMedia" class="tab-pane active">
						<center>
							<h2 class="text-primary header-title"><b>JASA SOSIAL MEDIA (AMAN + TANPA LOGIN AKUN)</b></h2>
						</center>
						<div style="text-align:left;">
							Sudah coba berbagai cara tapi jumlah <b>followers, subscribe, viewers, liker</b> atau <b>comment</b> di akun kamu nggak nambah-nambah? Coba gunakan tool dari situs <?php echo $data['short_title']; ?> dibawah ini untuk membuatmu populer di sosial media dengan cepat! Kami merekomendasikan tool bot auto sosial media terbaik dan aman yang bisa kamu coba. <br/><br/>
							Siapa sih yang nggak mau followers, subscribe, liker atau comment dengan instant dan cepat. Bahkan, kamu mungkin udah kenal dengan aplikasi auto followers gratis di internet untuk dapat ribuan followers gratis tiap hari!<br/><br/>
							Tapi ingat ya, jika <b style="color:red">memasukkan username dan password</b>, akunmu tidak akan aman karena situs yang kamu gunakan mungkin menyimpannya.<br/><br/>
							Nah, disini <?php echo $data['short_title']; ?> memberikan tool <b style="color:green">tanpa harus login akun</b>, <?php echo $data['short_title']; ?> tidak meminta password akun sosial mediamu, tapi hanya <b>username</b> saja jika ingin menambah <b>followers atau subscribe</b>, atau <b>link</b> postinganmu jika ingin menambah <b>like, viewers, atau comment</b> dengan cepat.
						</div>
						<br />
						<?php $cpr = $conn->query("SELECT * FROM kategori_layanan WHERE tipe = 'Sosial Media'"); ?>
						<?php while ($dpr = $cpr->fetch_assoc()) { ?>
							<br /><center><h3 class="header-title">App & Tool Sosial Media <?= $dpr['nama']; ?> Aman Tanpa Password</h3></center>
							<?php $cprl = $conn->query("SELECT * FROM layanan_sosmed WHERE kategori = '".$dpr['kode']."' ORDER BY harga ASC"); ?>
							<div class="table-responsive">
								<table class="table table-bordered table-hovered mb-1">
									<thead>
										<tr>
											<th width="100">ID</th>
											<th width="600">Jasa</th>
											<th width="100">Harga/1000</th>
											<th width="200">Harga API/1000</th>
										</tr>
									</thead>
									<tbody>
										<?php while($dprl = $cprl->fetch_assoc()) { ?>
											<?php
											if ($dprl['status'] == "Aktif") {
												$label = "primary";
											} else {
												$label = "danger";
											}
											?>
											<tr>
												<td><?= $dprl['service_id']; ?></td>
												<td>Bot Auto <?= $dprl['layanan']; ?> Tanpa Login/Password</td>
												<td>Rp.<?= number_format($dprl['harga'],0,',','.'); ?></td>
												<td>Rp <?= number_format($dprl['harga_api'],0,',','.'); ?></td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						<?php } ?>
					</div>

					<div id="AkunPremium" class="tab-pane">
						<center>
							<h3 class="text-primary"><b>AKUN PREMIUM</b></h3>
							Terakhir Diperbarui <?php echo date('j F Y'); ?>
						</center><br />
						<?php $cpr = $conn->query("SELECT * FROM kategori_layanan WHERE tipe IN ('NETFLIX', 'SPOTIFY', 'YT')"); ?>
						<?php while ($dpr = $cpr->fetch_assoc()) { ?>
							<br /><center><h4><?= $dpr['nama']; ?></h4></center>
							<?php $cprl = $conn->query("SELECT * FROM layanan_pulsa WHERE operator = '".$dpr['kode']."' ORDER BY harga ASC"); ?>
							<div class="table-responsive">
								<table class="table table-bordered table-hovered mb-1">
									<thead>
										<tr>
											<th width="200">ID</th>
											<th width="500">Nama Produk</th>
											<th width="200">Harga</th>
											<th width="200">Harga API</th>
											<th width="100">Status</th>
										</tr>
									</thead>
									<tbody>
										<?php while($dprl = $cprl->fetch_assoc()) { ?>
											<?php
											if ($dprl['status'] == "Normal") {
												$label = "primary";
											} else {
												$label = "danger";
											}
											?>
											<tr>
												<td><?= $dprl['service_id']; ?></td>
												<td><?= $dprl['layanan']; ?></td>
												<td>Rp <?= number_format($dprl['harga'],0,',','.'); ?></td>
												<td>Rp <?= number_format($dprl['harga_api'],0,',','.'); ?></td>
												<td><label class="btn btn-xs btn-<?= $label; ?>"><?= $dprl['status']; ?></label></td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						<?php } ?>
					</div>

					<div id="TopUpEMoney" class="tab-pane">
						<center>
							<h2 class="text-primary header-title"><b>LAYANAN TOP UP E-MONEY MURAH DAN TERPERCAYA</b></h2>
						</center>
						<div style="text-align:left;">
							E-Money pada saat ini dapat dikatakan menjadi kebutuhan bagi sebagian masyarakat Indonesia. Jumlah promosi yang ditawarkan oleh masing-masing penyedia layanan uang elektronik dan kemudahan cara pembayaran sebagai alasan utama mereka menggunakan uang elektronik.<br/><br/>
							Namun banyak yang kadang-kadang sulit untuk melakukan pengisian saldo <b>E-Money</b>. Banyak cara yang bisa dilakukan untuk mengisi saldo e-money. Salah satunya adalah melalui Panel <?php echo $data['short_title']; ?>. Isi ulang saldo E-Money kamu dengan nyaman dan praktis.<br/><br/>
							Aplikasi dan situs web <?php echo $data['short_title']; ?> sangat difasilitasi dalam segala bentuk pembayaran uang elektronik seperti <b>Top Up DANA, Go Pay, Go Pay Driver, OVO, Grab Pay, TIX ID, PAY Shopee, Mandiri E-Toll, LinkAja, Tapcash BNI</b> dan banyak lagi.
						</div>
						<br />
						<?php $cpr = $conn->query("SELECT * FROM kategori_layanan WHERE tipe = 'SALGO'"); ?>
						<?php while ($dpr = $cpr->fetch_assoc()) { ?>
							<br /><center><h3 class="header-title">Layanan Isi Ulang Saldo <?= $dpr['nama']; ?> Murah dan Terpercaya</h3></center>
							<?php $cprl = $conn->query("SELECT * FROM layanan_pulsa WHERE operator = '".$dpr['kode']."' ORDER BY harga ASC"); ?>
							<div class="table-responsive">
								<table class="table table-bordered table-hovered mb-1">
									<thead>
										<tr>
											<th width="100">ID</th>
											<th width="600">Produk</th>
											<th width="100">Harga</th>
										</tr>
									</thead>
									<tbody>
										<?php while($dprl = $cprl->fetch_assoc()) { ?>
											<?php
											if ($dprl['status'] == "Normal") {
												$label = "primary";
											} else {
												$label = "danger";
											}
											?>
											<tr>
												<td><?= $dprl['service_id']; ?></td>
												<td>Top Up Saldo <?= $dprl['layanan']; ?></td>
												<td>Rp <?= number_format($dprl['harga'],0,',','.'); ?></td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						<?php } ?>
					</div>

					<div id="TopUpGames" class="tab-pane">
						<center>
							<h2 class="text-primary header-title"><b>LAYANAN TOP UP GAMES TERCEPAT DAN TERMURAH</b></h2>
						</center>
						<div style="text-align:left;">
							Melakukan pembelian untuk item game dapat dilakukan dengan berbagai cara, yang paling umum adalah dengan kartu kredit atau transfer via bank. Jika kamu membeli dengan jumlah sedikit mungkin kamu dapat menggunakan transfer pulsa.<br/><br/>
							Di situs <?php echo $data['short_title']; ?>, kamu dapat <b>Top Up Games</b> dengan cepat dan mudah, kami menyediakan berbagai produk games dengan pembayaran melalui bank, e-payment, hingga pulsa transfer.<br/><br/>
							Dalam game kamu biasanya dapat membeli banyak barang-barang mulai dari hero, diamond, dan pernak-pernik lainnya. Sekarang banyak gamer indonesia yang mencari <b>Situs Top Up Game</b> yang murah dan terpercaya. <?php echo $data['short_title']; ?> adalah solusinya!
						</div>
						<br />
						<?php $cpr = $conn->query("SELECT * FROM kategori_layanan WHERE tipe = 'VGAME'"); ?>
						<?php while ($dpr = $cpr->fetch_assoc()) { ?>
							<br /><center><h3 class="header-title">Layanan Top Up <?= $dpr['nama']; ?> Cepat dan Terpercaya</h3></center>
							<?php $cprl = $conn->query("SELECT * FROM layanan_pulsa WHERE operator = '".$dpr['kode']."' ORDER BY harga ASC"); ?>
							<div class="table-responsive">
								<table class="table table-bordered table-hovered mb-1">
									<thead>
										<tr>
											<th width="100">ID</th>
											<th width="600">Nama</th>
											<th width="100">Harga</th>
										</tr>
									</thead>
									<tbody>
										<?php while($dprl = $cprl->fetch_assoc()) { ?>
											<?php
											if ($dprl['status'] == "Normal") {
												$label = "primary";
											} else {
												$label = "danger";
											}
											?>
											<tr>
												<td><?= $dprl['service_id']; ?></td>
												<td>Top Up <?= $dprl['layanan']; ?> Termurah</td>
												<td>Rp <?= number_format($dprl['harga'],0,',','.'); ?></td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						<?php } ?>
					</div>

					<div id="VoucherDigital" class="tab-pane">
						<center>
							<h3 class="text-primary"><b>TOP UP VOUCHER</b></h3>
							Terakhir Diperbarui <?php echo date('j F Y'); ?>
						</center><br />
						<?php $cpr = $conn->query("SELECT * FROM kategori_layanan WHERE tipe = 'VOUCHER'"); ?>
						<?php while ($dpr = $cpr->fetch_assoc()) { ?>
							<br /><center><h4><?= $dpr['nama']; ?></h4></center>
							<?php $cprl = $conn->query("SELECT * FROM layanan_pulsa WHERE operator = '".$dpr['kode']."' ORDER BY harga ASC"); ?>
							<div class="table-responsive">
								<table class="table table-bordered table-hovered mb-1">
									<thead>
										<tr>
											<th width="200">ID</th>
											<th width="500">Nama Produk</th>
											<th width="200">Harga</th>
											<th width="200">Harga API</th>
											<th width="100">Status</th>
										</tr>
									</thead>
									<tbody>
										<?php while($dprl = $cprl->fetch_assoc()) { ?>
											<?php
											if ($dprl['status'] == "Normal") {
												$label = "primary";
											} else {
												$label = "danger";
											}
											?>
											<tr>
												<td><?= $dprl['service_id']; ?></td>
												<td><?= $dprl['layanan']; ?></td>
												<td>Rp. <?= number_format($dprl['harga'],0,',','.'); ?></td>
												<td>Rp. <?= number_format($dprl['harga_api'],0,',','.'); ?></td>
												<td><label class="btn btn-xs btn-<?= $label; ?>"><?= $dprl['status']; ?></label></td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						<?php } ?>
					</div>

					<div id="TokenPLN" class="tab-pane">
						<center>
							<h2 class="text-primary header-title"><b>LAYANAN TOKEN PLN HARGA TERMURAH</b></h2>
						</center>
						<div style="text-align:left;">
							Beli token listrik (PLN) di Agen kini jadi bagian yang tak terlepas dari keseharian. Tak heran banyak yang menjadi agen token listrik PLN untuk mempermudah Top-Up listrik rumah. Apalagi, menjadi agen token listrik PLN bisa memberikan keuntungan yang lumayan.<br/><br/>
							Tertarik untuk <b>Jadi Agen Penjual Token Listrik PLN?</b> Tak perlu bingung karena kamu juga bisa menjadi agen penjual token listrik PLN dengan mudah dan praktis.<br/><br/>
							Cukup dengan satu aplikasi <?php echo $data['short_title']; ?>, kamu bisa mulai menjadi agen/reseller, dan juga member kami dengan membeli <b>Token PLN Harga Termurah</b>. Berikut daftar harga token listrik yang kami sediakan.
						</div>
						<br />
						<?php $cpr = $conn->query("SELECT * FROM kategori_layanan WHERE tipe = 'TOKENPLN'"); ?>
						<?php while ($dpr = $cpr->fetch_assoc()) { ?>
							<br /><center><h3 class="header-title">Distributor Token <?= $dpr['nama']; ?> Murah dan Terpercaya</h3></center>
							<?php $cprl = $conn->query("SELECT * FROM layanan_pulsa WHERE operator = '".$dpr['kode']."' ORDER BY harga ASC"); ?>
							<div class="table-responsive">
								<table class="table table-bordered table-hovered mb-1">
									<thead>
										<tr>
											<th width="100">ID</th>
											<th width="600">Nama</th>
											<th width="100">Harga</th>
										</tr>
									</thead>
									<tbody>
										<?php while($dprl = $cprl->fetch_assoc()) { ?>
											<?php
											if ($dprl['status'] == "Normal") {
												$label = "primary";
											} else {
												$label = "danger";
											}
											?>
											<tr>
												<td><?= $dprl['service_id']; ?></td>
												<td>Token <?= $dprl['layanan']; ?> Termurah</td>
												<td>Rp <?= number_format($dprl['harga'],0,',','.'); ?></td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						<?php } ?>
					</div>

					<div id="PaketInternet" class="tab-pane">
						<center>
							<h2 class="text-primary header-title"><b>PAKET INTERNET ALL OPERATOR 3G & 4G (24 JAM)</b></h2>
						</center>
						<div style="text-align:left;">
							Kehadiran paket internet yang dijual langsung secara online tentu saja sangat mudah untuk menjalani aktivitas sehari-hari. Jika aktivitas kamu membutuhkan komunikasi mobile dengan internet, ini akan merepotkan jika paket data internet kamu tiba-tiba habis.<br/><br/>
							Sebuah layanan pembelian paket internet online di <?php echo $data['short_title']; ?> bisa kamu coba, kamu tidak perlu repot untuk mendaftar paket internet dengan pulsa. Hanya memilih paket internet yang tersedia di <?php echo $data['short_title']; ?> dan bayar dengan harga murah.<br/><br/>
							<?php echo $data['short_title']; ?> menyediakan Paket Data Internet Telkomsel, Indosat, XL, 3 (Tri) dan Smart fren, dengan harga termurah dan proses cepat. Berbagai pillihan jenis paket data, durasi, dan kuota membuat kamu lebih leluasa memilih.
						</div>
						<br />
						<?php $cpr = $conn->query("SELECT * FROM kategori_layanan WHERE tipe = 'PKIN'"); ?>
						<?php while ($dpr = $cpr->fetch_assoc()) { ?>
							<br /><center><h3 class="header-title">Paket Internet Murah <?= $dpr['nama']; ?> 24 Jam Terbaru</h3></center>
							<?php $cprl = $conn->query("SELECT * FROM layanan_pulsa WHERE operator = '".$dpr['kode']."' ORDER BY harga ASC"); ?>
							<div class="table-responsive">
								<table class="table table-bordered table-hovered mb-1">
									<thead>
										<tr>
											<th width="100">ID</th>
											<th width="600">Nama</th>
											<th width="100">Harga</th>
										</tr>
									</thead>
									<tbody>
										<?php while($dprl = $cprl->fetch_assoc()) { ?>
											<?php
											if ($dprl['status'] == "Normal") {
												$label = "primary";
											} else {
												$label = "danger";
											}
											?>
											<tr>
												<td><?= $dprl['service_id']; ?></td>
												<td>Paket Internet Murah <?= $dprl['layanan']; ?>  3G 4G Terbaru</td>
												<td>Rp <?= number_format($dprl['harga'],0,',','.'); ?></td>

											</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						<?php } ?>
					</div>

					<div id="PulsaReguler" class="tab-pane">
						<center>
							<h2 class="text-primary header-title"><b>PULSA REGULER TERMURAH ALL OPERATOR</b></h2>
						</center>
						<div style="text-align:left;">
							Butuh pulsa mendadak untuk berbagai operator dan jumlah? Isi pulsa bisa jadi lebih cepat, mudah, dan lebih praktis. KM menyediakan berbagai metode pembayaran. Sekarang kamu order pembelian pulsa dengan menggunakan <?php echo $data['short_title']; ?>, dengan pilihan semua operator seluler di Indonesia, banyak jenis pulsa dan harga jual yang sesuai dengan kebutuhan kamu.<br/><br/>
							Membeli pulsa online di <?php echo $data['short_title']; ?> lebih mudah dan terpercaya, hanya dengan memasukkan nomor dan nominal, pulsa segera dikirim ke telepon dalam waktu singkat. Mengisi pulsa dapat dilakukan kapan saja, dimana saja melalui browser ponsel, smartphone dan tablet, PC / laptop di kantor dan di rumah dengan aplikasi <?php echo $data['short_title']; ?> versi Web dan Mobile.<br/><br/>
							Dapatkan harga pulsa termurah, diskon, hingga cashback setiap pembelian. Di <?php echo $data['short_title']; ?> kamu bisa beli pulsa hp lewat internet tanpa perlu pusing waktu & tempat, karna <?php echo $data['short_title']; ?> online 24 jam. Metode pembayaran pulsa bisa dengan Paypal, Cryptocurrency, Voucher Indodax, E-Money, dan Bank.
						</div>
						<br />
						<?php $cpr = $conn->query("SELECT * FROM kategori_layanan WHERE tipe = 'PULSA'"); ?>
						<?php while ($dpr = $cpr->fetch_assoc()) { ?>
							<br /><center><h3 class="header-title">Beli Pulsa <?= $dpr['nama']; ?> Online Termurah dan Cepat</h3></center>
							<?php $cprl = $conn->query("SELECT * FROM layanan_pulsa WHERE operator = '".$dpr['kode']."' ORDER BY harga ASC"); ?>
							<div class="table-responsive">
								<table class="table table-bordered table-hovered mb-1">
									<thead>
										<tr>
											<th width="100">ID</th>
											<th width="600">Nama</th>
											<th width="100">Harga</th>
										</tr>
									</thead>
									<tbody>
										<?php while($dprl = $cprl->fetch_assoc()) { ?>
											<?php
											if ($dprl['status'] == "Normal") {
												$label = "primary";
											} else {
												$label = "danger";
											}
											?>
											<tr>
												<td><?= $dprl['service_id']; ?></td>
												<td>Pulsa <?= $dprl['layanan']; ?> Termurah</td>
												<td>Rp <?= number_format($dprl['harga'],0,',','.'); ?></td>

											</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						<?php } ?>
					</div>

					<div id="TeleponSMS" class="tab-pane">
						<center>
							<h2 class="text-primary header-title"><b>PAKET TELEPON & SMS ALL OPERATOR TERBARU</b></h2>
						</center>
						<div style="text-align:left;">
							Kehadiran paket telepon dan sms yang dijual langsung secara online tentu saja sangat memudahkan untuk menjalani aktivitas sehari-hari. Jika aktivitas kamu membutuhkan komunikasi mobile, ini akan merepotkan jika paket telepon atau sms di ponselmu tiba-tiba habis.<br/><br/>
							Sebuah layanan pembelian paket telepon dan sms secara online ini bisa kamu coba, kamu tidak perlu repot untuk mendaftar paket dengan pulsa. Hanya memilih paket telepon dan sms yang tersedia dan bayar dengan harga termurah.<br/><br/>
							<?php echo $data['short_title']; ?> menyediakan Paket telepon dan sms untuk operator Telkomsel, Indosat, XL, 3 (Tri) dan Smart fren, dengan harga murah dan proses cepat. Berbagai pillihan jenis paket dan durasi membuat kamu lebih leluasa memilih sesuai kebutuhan.
						</div>
						<br />
						<?php $cpr = $conn->query("SELECT * FROM kategori_layanan WHERE tipe = 'PKSMS'"); ?>
						<?php while ($dpr = $cpr->fetch_assoc()) { ?>
							<br /><center><h3 class="header-title">Paket <?= $dpr['nama']; ?> Harian/Mingguan/Bulanan</h3></center>
							<?php $cprl = $conn->query("SELECT * FROM layanan_pulsa WHERE operator = '".$dpr['kode']."' ORDER BY harga ASC"); ?>
							<div class="table-responsive">
								<table class="table table-bordered table-hovered mb-1">
									<thead>
										<tr>
											<th width="100">ID</th>
											<th width="600">Nama</th>
											<th width="100">Harga</th>
										</tr>
									</thead>
									<tbody>
										<?php while($dprl = $cprl->fetch_assoc()) { ?>
											<?php
											if ($dprl['status'] == "Normal") {
												$label = "primary";
											} else {
												$label = "danger";
											}
											?>
											<tr>
												<td><?= $dprl['service_id']; ?></td>
												<td>Paket <?= $dprl['layanan']; ?> Terbaru</td>
												<td>Rp <?= number_format($dprl['harga'],0,',','.'); ?></td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						<?php } ?>
					</div>

					<div id="ProvideGlobal" class="tab-pane">
						<center>
							<h2 class="text-primary header-title"><b>PRODUK DIGITAL PROVIDER GLOBAL</b></h2>
						</center>
						<div style="text-align:left;">
							Selain sebagai penyedia produk digital dari indonesia, kami juga menyediakan berbagai layanan jasa Top Up, E-Payment, dan E-Wallet untuk produk digital di Asia.<br/><br/>
							Layanan dari <?php echo $data['short_title']; ?> akan terus dikembangkan menjadi lebih baik, agar kami bisa menyediakan banyak produk digital untuk Provider Global. Saat ini terdapat layanan untuk operator Digi, Maxis, Celcom, Umobile, Tunetalk, dan Steam Wallet.<br/><br/>
							Dan layanan perbankan dari provider indonesia dalam kategori ini yaitu Top Up saldo BRIZZI dari Bank BRI.
						</div>
						<br />
						<?php $cpr = $conn->query("SELECT * FROM kategori_layanan WHERE tipe = 'LAINYA'"); ?>
						<?php while ($dpr = $cpr->fetch_assoc()) { ?>
							<br /><center><h3 class="header-title">Layanan & Produk <?= $dpr['nama']; ?> Termurah</h3></center>
							<?php $cprl = $conn->query("SELECT * FROM layanan_pulsa WHERE operator = '".$dpr['kode']."' ORDER BY harga ASC"); ?>
							<div class="table-responsive">
								<table class="table table-bordered table-hovered mb-1">
									<thead>
										<tr>
											<th width="100">ID</th>
											<th width="600">Nama</th>
											<th width="100">Harga</th>
										</tr>
									</thead>
									<tbody>
										<?php while($dprl = $cprl->fetch_assoc()) { ?>
											<?php
											if ($dprl['status'] == "Normal") {
												$label = "primary";
											} else {
												$label = "danger";
											}
											?>
											<tr>
												<td><?= $dprl['service_id']; ?></td>
												<td>Top Up <?= $dprl['layanan']; ?> Terpercaya</td>
												<td>Rp <?= number_format($dprl['harga'],0,',','.'); ?></td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						<?php } ?>
					</div>

				</div>

			</div>
		</div>
	</div>

	<!--Filter Kategori Navigasi v2-->
	<script>
		$(document).ready(function(){
			$('#filter_kategori').on('change',function(){
				var selected = this.value;

				$('[class="tab-pane active"]').removeClass('active');
				$('#'+selected).addClass('active');
			});
		});
	</script>

<?php
require '../lib/footer.php';
?>