<!--Scroll Indicator Load-->
<div class='progress-container'>
	<div class='progress-bar' id='progressbar'/>
</div>

<script>
//<![CDATA[
window.addEventListener("scroll", myFunction);
function myFunction() {
	var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
	var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
	var scrolled = (winScroll / height) * 100;
	document.getElementById("progressbar").style.width = scrolled + "%";
}
//]]>
</script>

<!--Nofollow-->
<script type="text/javascript">
//<![CDATA[
$("a").filter(function() {
	return this.hostname && this.hostname !== location.hostname
}).attr("rel", "dofollow").attr("target", "_blank");
//]]>
</script>