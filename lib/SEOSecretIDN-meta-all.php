<!--Viewport -->
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="upgrade-insecure-requests" http-equiv="Content-Security-Policy"/>

<!--Canonical-->
<meta content="all" name="robots"/>
<link href="<?php echo $config['web']['url'];?>home" rel="home"/>
<?php
$fullurl = ($_SERVER['PHP_SELF']);
$trimmed = trim($fullurl, ".php");
$canonical = rtrim($trimmed, '/' . '/?');
?>
<link href="https://borneocell.online<?php echo $canonical ?>" rel="canonical"/>

<!--Author-->
<meta content="<?php echo $data['short_title'];?>" name="author" />
<meta content="MFA" name="publisher"/>

<!--verification-->
<meta content="" name="yandex-verification"/>
<meta content="" name="p:domain_verify"/>
<meta content="" name="msvalidate.01"/>
<meta content="" name="google-site-verification"/>
<meta content="" name="dmca-site-verification"/>
<meta content="" name="facebook-domain-verification"/>

<!--Webapp-->
<link href="<?php echo $config['web']['url'];?>manifest.json" rel="manifest"/>
<meta content="<?php echo $config['web']['url'];?>" name="msapplication-starturl"/>
<meta content="<?php echo $config['web']['url'];?>" name="start_url"/>
<meta content="<?php echo $data['short_title'];?>" name="application-name"/>
<meta content="<?php echo $data['short_title'];?>" name="apple-mobile-web-app-title"/>
<meta content="<?php echo $data['short_title'];?>" name="msapplication-tooltip"/>
<meta content="#0D8BF2" name="theme_color"/>
<meta content="#0D8BF2" name="theme-color"/>
<meta content="#FFFFFF" name="background_color"/>
<meta content="#0D8BF2" name="msapplication-navbutton-color"/>
<meta content="#0D8BF2" name="msapplication-TileColor"/>
<meta content="#0D8BF2" name="apple-mobile-web-app-status-bar-style"/>
<meta content="true" name="mssmarttagspreventparsing"/>
<meta content="yes" name="apple-mobile-web-app-capable"/>
<meta content="yes" name="mobile-web-app-capable"/>
<meta content="yes" name="apple-touch-fullscreen"/>
<link href="<?php echo $config['web']['url'];?>assets/images/kincaimedia/favicon.ico" rel="apple-touch-icon"/>
<link href="<?php echo $config['web']['url'];?>assets/images/kincaimedia/favicon.ico" rel="shortcut icon icons" type="image/x-icon"/>
<link href="<?php echo $config['web']['url'];?>assets/images/kincaimedia/kincaimedia32.png" rel="icon icons" sizes="32x32"/>
<meta content="<?php echo $config['web']['url'];?>assets/images/kincaimedia/kincaimedia144.png" name="msapplication-TileImage"/>
<link href="<?php echo $config['web']['url'];?>assets/images/kincaimedia/kincaimedia180.png" rel="apple-touch-icon"/>
<link href="<?php echo $config['web']['url'];?>assets/images/kincaimedia/kincaimedia48.png" rel="icon icons" sizes="48x48"/>
<link href="<?php echo $config['web']['url'];?>assets/images/kincaimedia/kincaimedia72.png" rel="icon icons" sizes="72x72"/>
<link href="<?php echo $config['web']['url'];?>assets/images/kincaimedia/kincaimedia96.png" rel="icon icons" sizes="96x96"/>
<link href="<?php echo $config['web']['url'];?>assets/images/kincaimedia/kincaimedia168.png" rel="icon icons" sizes="168x168"/>
<link href="<?php echo $config['web']['url'];?>assets/images/kincaimedia/kincaimedia192.png" rel="icon icons" sizes="192x192"/>
<link href="<?php echo $config['web']['url'];?>assets/images/kincaimedia/kincaimedia512.png" rel="icon icons" sizes="512x512"/>

<!--Location-->
<meta content="ID" name="geo.region"/>
<meta content="Indonesia" name="geo.country"/>
<meta content="Indonesia" name="geo.placename"/>
<meta content="x;x" name="geo.position"/>
<meta content="x,x" name="ICBM"/>

<!--OG1-->
<meta content="website" property="og:type"/>
<meta content="https://azfapanel.com<?php echo $canonical ?>" property="og:url"/>
<meta content="<?php echo $data['short_title'];?>" property="og:site_name"/>
<meta content="1087935584575609" property="fb:app_id"/>
<meta content="1087935584575609" property="fb:pages"/>
<meta content="1087935584575609" property="fb:admins"/>
<meta content="1087935584575609" property="fb:profile_id"/>
<meta content="<?php echo $data['short_title'];?>" property="article:author"/>
<meta content="https://azfapanel.com<?php echo $canonical ?>" property="twitter:url"/>
<meta content="id_ID" property="og:locale"/>
<meta content="en_US" property="og:locale:alternate"/>
<meta content="true" property="og:rich_attachment"/>
<meta content="true" property="pinterest-rich-pin"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="@MyCodingXD" name="twitter:site"/>
<meta content="@MyCodingXD" name="twitter:creator"/>