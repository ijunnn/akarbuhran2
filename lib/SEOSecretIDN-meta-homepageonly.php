<!--Title-->
<title><?php echo $data['title']; ?></title>
<meta name="description" content="<?php echo $data['short_title']; ?> - <?php echo $data['deskripsi_web']; ?>"/>
<meta name="keywords" content="<?php echo $data['short_title']; ?>, Auto subscribe yt, Auto follower ig, SMM Panel indo, Server pulsa h2h, Top up game, SMM kincai media"/>

<!--OG-->
<meta content="website" property="og:type"/>
<meta content="<?php echo $data['title']; ?>" property="og:title"/>
<meta content="<?php echo $data['short_title']; ?> - <?php echo $data['deskripsi_web']; ?>" property="og:description"/>
<meta content="<?php echo $config['web']['url'];?>home" property="og:url"/>
<meta content="<?php echo $data['short_title'];?>" property="og:site_name"/>
<meta content="<?php echo $data['title']; ?>" property="og:headline"/>
<meta content="<?php echo $config['web']['url'];?>assets/images/kincaimedia/kincaimedia512.png" property="og:image"/>
<meta content="512" property="og:image:width"/>
<meta content="512" property="og:image:height"/>
<meta content="id_ID" property="og:locale"/>
<meta content="en_US" property="og:locale:alternate"/>
<meta content="true" property="og:rich_attachment"/>
<meta content="true" property="pinterest-rich-pin"/>
<meta content="1087935584575609" property="fb:app_id"/>
<meta content="1087935584575609" property="fb:pages"/>
<meta content="1087935584575609" property="fb:admins"/>
<meta content="1087935584575609" property="fb:profile_id"/>
<meta content="<?php echo $data['short_title'];?>" property="article:author"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="@BlackExpoIndo" name="twitter:site"/>
<meta content="@BlackExpoIndo" name="twitter:creator"/>
<meta content="<?php echo $config['web']['url'];?>home" property="twitter:url"/>
<meta content="<?php echo $data['title'];?>" property="twitter:title"/>
<meta content="<?php echo $data['short_title']; ?> - <?php echo $data['deskripsi_web']; ?>" property="twitter:description"/>
<meta content="<?php echo $config['web']['url'];?>assets/images/kincaimedia/kincaimedia512.png" property="twitter:image"/>