<?php
function tanggal_indo($tanggal)
{
    $bulan = array (1 =>   'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            );
    $split = explode('-', $tanggal);
    return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
}

function filter($data){

$filter = stripslashes(strip_tags(htmlspecialchars(htmlentities($data,ENT_QUOTES))));

return $filter;

}

function acak($length) {
	$str = "";
	$karakter = array_merge(range('A','Z'), range('a','z'), range('0','9'));
	$max_karakter = count($karakter) - 1;
	for ($i = 0; $i < $length; $i++) {
		$rand = mt_rand(0, $max_karakter);
		$str .= $karakter[$rand];
	}
	return $str;
}

function acak_nomor($length) {
	$str = "";
	$karakter = array_merge(range('0','9'));
	$max_karakter = count($karakter) - 1;
	for ($i = 0; $i < $length; $i++) {
		$rand = mt_rand(0, $max_karakter);
		$str .= $karakter[$rand];
	}
	return $str;
}

function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'Tahun',
        'm' => 'Bulan',
        'w' => 'Minggu',
        'd' => 'Hari',
        'h' => 'Jam',
        'i' => 'Menit',
        's' => 'Detik',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? '' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' Yang Lalu' : 'Baru Saja';
}

function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP')) {
        $ipaddress = getenv('HTTP_CLIENT_IP');
    } elseif (getenv('HTTP_X_FORWARDED_FOR')) {
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    } elseif (getenv('HTTP_X_FORWARDED')) {
        $ipaddress = getenv('HTTP_X_FORWARDED');
    } elseif (getenv('HTTP_FORWARDED_FOR')) {
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    } elseif (getenv('HTTP_FORWARDED')) {
        $ipaddress = getenv('HTTP_FORWARDED');
    } elseif (getenv('REMOTE_ADDR')) {
        $ipaddress = getenv('REMOTE_ADDR');
    } else {
        $ipaddress = 'UNKNOWN';
    }
    return $ipaddress;
}

function validate_date($date) {
    $d = DateTime::createFromFormat('Y-m-d', $date);
    return $d && $d->format('Y-m-d') == $date;
}

function infojson($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $data = curl_exec($ch);
        curl_close($ch);
                return $data;
}

function Show($tabel, $limit) {
        global $conn;
        $CallData = mysqli_query($conn, "SELECT * FROM ".$tabel." WHERE ".$limit);
        $ThisData = mysqli_fetch_assoc($CallData);
        return $ThisData;
}

function followers_count($data){
    $id = file_get_contents("https://instagram.com/web/search/topsearch/?query=".$data);
    $id = json_decode($id, true);
    $count = $id['users'][0]['user']['follower_count'];
    return $count;
}

function likes_count($data){
    $id = file_get_contents("".$data."?&__a=1");
    $id = json_decode($id, true);
    $count = $id['graphql']['shortcode_media']['edge_media_preview_like']['count'];
    return $count;
}

function views_count($data){
    $id = file_get_contents("".$data."?&__a=1");
    $id = json_decode($id, true);
    $count = $id['graphql']['shortcode_media']['video_view_count'];
    return $count;
}

function daftar($data) {
    global $conn;

    $nama = $conn->real_escape_string(filter(trim($data['nama'])));
    $email = $conn->real_escape_string(filter(trim($data['email'])));
    //$nomer = $conn->real_escape_string(filter(trim($data['nomer'])));
    $username = filter($data['username']);
    $password = $conn->real_escape_string(trim($data['password']));
    $password2 = $conn->real_escape_string(trim($data['password2']));

    $cek_nama = $conn->query("SELECT * FROM users WHERE nama = '$nama'");
    $cek_email = $conn->query("SELECT * FROM users WHERE email = '$email'");
    //$cek_nomer = $conn->query("SELECT * FROM users WHERE nomer = '$nomer'");
    $cek_username = $conn->query("SELECT * FROM users WHERE username = '$username'");
    if (!$nama || !$email || !$username || !$password) {
            $_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Pendaftaran gagal!', 'pesan' => 'Silahkan lengkapi bidang berikut :<br/> - Nama Lengkap<br/> - Email Aktif<br/> - Username Login<br/> - Password Akun<br/> - Konfirmasi Password');
    return false;
    } else if ($cek_username->num_rows > 0) {
            $_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Pendaftaran gagal!', 'pesan' => 'Username <strong>'.$username.' </strong> telah terdaftar'); 
    return false;
    } else if ($cek_email->num_rows > 0) {
            $_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Pendaftaran gagal!', 'pesan' => 'Email <strong> '.$email.' </strong> telah terdaftar'); 
    return false;
    } elseif (strlen($username) < 5) {
            $_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Pendaftaran gagal!', 'pesan' => 'Username minimal 5 karakter');
    return false;
    } elseif (strlen($password) < 5) {
            $_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Pendaftaran gagal!', 'pesan' => 'Password minimal 5 karakter');
    return false;
    } else if ($password <> $password2){
            $_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Pendaftaran gagal!', 'pesan' => 'Konfirmasi password tidak sesuai');
        return false;
    }

    $hash_pass = password_hash($password, PASSWORD_DEFAULT);
    $api_key =  acak(32);
    $terdaftar = date("Y-m-d H:i:s");

    $conn->query("INSERT INTO users VALUES (NULL, '$nama', '$email', '$username', '$hash_pass', '0', '0', 'Member', 'Aktif', '$api_key', 'Pendaftaran Gratis', '$terdaftar', '0','')");
    return mysqli_affected_rows($conn);
}

function lupa_password($data) {
    global $conn;

    //Call email form validation
    $PostEmail = $conn->real_escape_string(filter(trim($_POST['email'])));
    $cek_email = $conn->query("SELECT * FROM users WHERE email = '$PostEmail'");
    $email = $cek_email->fetch_assoc();

    //Call username form validation
    $PostUsername = $conn->real_escape_string(filter(trim($_POST['username'])));
    $cek_username = $conn->query("SELECT * FROM users WHERE username = '$PostUsername'");
    $user = $cek_username->fetch_assoc();

    //Send new pass to email by username or email method
    $CallPostEmailUsername1 = $conn->real_escape_string(filter(trim($_POST['email'])));
    $CallPostEmailUsername2 = $conn->real_escape_string(filter(trim($_POST['username'])));
    $cek_emailusername = $conn->query("SELECT * FROM users WHERE username = '$CallPostEmailUsername2' OR email = '$CallPostEmailUsername1'");
    $useremail = $cek_emailusername->fetch_assoc();

    //Validation form
    if (!$PostEmail && !$PostUsername) {
            $_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Permintaan gagal!', 'pesan' => 'Silahkan lengkapi bidang berikut :<br /> - Email<br/> <b>Atau</b><br/> - Username');
            return true;
    } else if ($cek_email->num_rows == 0 && !$PostUsername) {
            $_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Permintaan gagal!', 'pesan' => 'Email <strong>'.$PostEmail.' </strong> tidak terdaftar'); 
            return true;
    } else if ($cek_username->num_rows == 0 && !$PostEmail) {
            $_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Permintaan gagal!', 'pesan' => 'Username <strong>'.$PostUsername.' </strong> tidak terdaftar');
            return true;
    } else if ($cek_email->num_rows == 0 && $cek_username->num_rows == 0) {
            $_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Permintaan gagal!', 'pesan' => 'Email <strong>'.$PostEmail.' </strong> atau Username <strong>'.$PostUsername.' </strong> tidak terdaftar');
            return true;
    }

    $acakin_password = acak(10).acak_nomor(10);
    $hash_pass = password_hash($acakin_password, PASSWORD_DEFAULT);
    $tujuan = $useremail['email'];
    $pesannya = "
                <p>Hai ".$useremail['username']."</p>
                <p>Anda telah membuat permintaan <b>Reset Password</b> untuk akun ".$useremail['email'].".</p>
                <p>Gunakan <b>Kode Unik</b> dibawah ini sebagai password baru, kemudian login dan mengatur ulang kata sandi di menu pengaturan.</p>
                <p>
                Email: ".$useremail['email']."<br/>
                Username: ".$useremail['username']."<br/>
                Password: $acakin_password<br/>
                Halaman Login: ".'<a href="https://kincaiseluler.store/auth/login">'." Member KS Panel</a>
                </p>
                <p>
                <b>Temukan Kami</b><br/>
                Web: ".'<a href="http://www.boostnaa.com">'." boostnaa.com</a><br/>
                Media: ".'<a href="http://www.boostnaa.com">'." boostnaa.com</a><br/>
                Developer: ".'<a href="http://www.boostnaa.com">'." boostnaa.com</a><br/>
                Company: ".'<a href="http://www.boostnaa.com">'." boostnaa.com</a>
                <hr>
                ";
    $subjek = "Password akun Anda diubah";
    $header = "From:Administrator info@boostnaa.com\r\n";
    $header .= "Cc:info@boostnaa.com\r\n";
    $header .= "MIME-Version:1.0\r\n";
    $header .= "Content-type:text/html\r\n";
    $send = mail ($tujuan, $subjek, $pesannya, $header);
    $conn->query("UPDATE users SET password = '$hash_pass', random_kode = '$acakin_password' WHERE email = '".$useremail['email']."' ");
    return mysqli_affected_rows($conn);
}