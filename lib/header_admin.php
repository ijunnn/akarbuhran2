<?php
require 'session_login_admin.php';
require 'database.php';
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="<?php echo $config['web']['meta']['description'] ?>">
	<meta name="keywords" content="<?php echo $config['web']['meta']['keywords'] ?>">
	<meta name="author" content="<?php echo $config['web']['meta']['author'] ?>">
	<title><?php echo $data['short_title'];?></title>
	<link href="/assets/plugins/morris/morris.css" rel="stylesheet" />
	<link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="/assets/css/icons.css" rel="stylesheet" type="text/css" />
	<link href="/assets/css/style.css" rel="stylesheet" type="text/css" />
	<script src="/assets/js/modernizr.min.js"></script>
	<script src="/assets/js/jquery.min.js"></script>
	<script src="/assets/js/"></script>
	<script src="/assets/js/popper.min.js"></script>
	<script src="/assets/js/bootstrap.min.js"></script>
	<script src="/assets/js/waves.js"></script>
	<script src="/assets/js/jquery.slimscroll.js"></script>
	<script src="/assets/plugins/morris/morris.min.js"></script>
	<script src="/assets/plugins/raphael/raphael-min.js"></script>
	<style type="text/css">.hide{display:none!important}.show{display:block!important}</style>
	<script type="text/javascript">
		function modal_open(type, url) {
			$('#modal').modal('show');
			if (type == 'add') {
				$('#modal-title').html('<i class="fa fa-plus-square"></i> Tambah Data');
			} else if (type == 'edit') {
				$('#modal-title').html('<i class="fa fa-edit"></i> Ubah Data');
			} else if (type == 'delete') {
				$('#modal-title').html('<i class="fa fa-trash"></i> Hapus Data');
			} else if (type == 'detail') {
				$('#modal-title').html('<i class="fa fa-search"></i> Detail Data');
			} else {
				$('#modal-title').html('Empty');
			}
			$.ajax({
				type: "GET",
				url: url,
				beforeSend: function() {
					$('#modal-detail-body').html('Sedang memuat...');
				},
				success: function(result) {
					$('#modal-detail-body').html(result);
				},
				error: function() {
					$('#modal-detail-body').html('Terjadi kesalahan.');
				}
			});
			$('#modal-detail').modal();
		}
	</script>
</head>
<body>

	<header id="topnav">
		<div class="topbar-main">
			<div class="container-fluid">
				<div class="logo">
					<a href="<?php echo $config['web']['url'];?>" class="logo">
						<span class="logo-small"><i class="fa fa-shopping-cart"></i></span>
						<span class="logo-large"><i class="fa fa-shopping-cart"></i> <?php echo $data['short_title'];?></span>
					</a>
				</div>
				<div class="menu-extras topbar-custom">
					<ul class="list-unstyled topbar-right-menu float-right mb-0">
						<li class="menu-item">
							<a class="navbar-toggle nav-link">
								<div class="lines">
									<span></span>
									<span></span>
									<span></span>
								</div>
							</a>
						</li>
						<?php
            if (isset($_SESSION['user'])) {
                ?>
							<li style="padding: 0 10px;">
								<span class="nav-link">Saldo: Rp <?php echo number_format($data_user['saldo'],0,',','.'); ?></span>
							</li>
							<li class="dropdown notification-list">
								<a class="nav-link dropdown-toggle waves-effect nav-user" data-toggle="dropdown" href="#" role="button"
								aria-haspopup="false" aria-expanded="false">
								<img src="/assets/images/users/user2.png" alt="user" class="rounded-circle"> <span class="ml-1 pro-user-name"><?php echo $sess_username; ?><i class="mdi mdi-chevron-down"></i> </span> </span>
							</a>
							<div class="dropdown-menu dropdown-menu-right profile-dropdown">
								<a href="/user/setting" class="dropdown-item notify-item"><i class="fa fa-gear fa-fw"></i> <span>Pengaturan Akun</span></a>
								<a href="/logout" class="dropdown-item notify-item"><i class="fa fa-sign-out fa-fw"></i> <span>Keluar</span></a>
							</div>
						</li>
						<?php 
					}
					?>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>

        <div class="navbar-custom">
            <div class="container-fluid">
                <div id="navigation">
                    <!-- Navigation Menu-->
                    <ul class="navigation-menu">                            
                        <li class="has-submenu">
                            <a href="#"><i class="fi-air-play"></i> <span>  Dashboard</span> </a>
                            <ul class="submenu">
                                <li><a href="<?php echo $config['web']['url'];?>admin-dashboard/action-provider">Provider</a></li>
                                <li><a href="<?php echo $config['web']['url'];?>admin-dashboard/provider">Api</a></li>
                                <li><a href="<?php echo $config['web']['url'];?>">Halaman Member</a></li>
                            </ul>
                        </li>
                        
                        <li class="has-submenu">
                            <a href="<?php echo $config['web']['url'];?>admin-dashboard/pengguna"><i class="mdi mdi-account-multiple"></i> <span> Pengguna</span> </a>
                        </li>  

                        <li class="has-submenu">
                            <a href="<?php echo $config['web']['url'];?>admin-dashboard/tiket"><i class="dripicons-ticket"></i> <span>Tiket</span>
                                <?php if (mysqli_num_rows($AllTiketUsersPending) !== 0) { ?><span class="badge badge-warning"><?php echo mysqli_num_rows($AllTiketUsersPending); ?></span><?php } ?> 
                                <?php if (mysqli_num_rows($AllTiketUsersWaiting) !== 0) { ?><span class="badge badge-info"><?php echo mysqli_num_rows($AllTiketUsersWaiting); ?></span><?php } ?> 
                            </a>
                        </li>

                        <li class="has-submenu">
                            <a href="#"><i class="mdi mdi-wallet"></i> <span> Deposit </span>
                            <?php if (mysqli_num_rows($AllDepositUsersBank) !== 0) { ?><span class="badge badge-success"><?php echo mysqli_num_rows($AllDepositUsersBank); ?></span> <?php } ?>
                            <?php if (mysqli_num_rows($AllDepositUsersEmoney) !== 0) { ?><span class="badge badge-primary"><?php echo mysqli_num_rows($AllDepositUsersEmoney); ?></span> <?php } ?>
                            <?php if (mysqli_num_rows($AllDepositUsersVoucher) !== 0) { ?><span class="badge badge-danger"><?php echo mysqli_num_rows($AllDepositUsersVoucher); ?></span> <?php } ?>
                            <?php if (mysqli_num_rows($AllDepositUsersLain) !== 0) { ?><span class="badge badge-warning"><?php echo mysqli_num_rows($AllDepositUsersLain); ?></span> <?php } ?>
                            </a>
                            <ul class="submenu">
                                <li><a href="<?php echo $config['web']['url'];?>admin-dashboard/metode-deposit">Kelola Deposit</a></li>
                                <li><a href="<?php echo $config['web']['url'];?>admin-dashboard/mutasi-bca">Mutasi BCA</a></li>
                                <li><a href="<?php echo $config['web']['url'];?>admin-dashboard/deposit">Daftar Deposit</a></li>
                                <li><a href="<?php echo $config['web']['url'];?>admin-dashboard/deposit-voucher">Deposit Voucher</a></li>
                                <li><a href="<?php echo $config['web']['url'];?>admin-dashboard/voucher">Buat Voucher</a></li>
                            </ul>
                        </li>

                        <li class="has-submenu">
                            <a href="<?php echo $config['web']['url'];?>admin-dashboard/sosial-media"><i class="mdi mdi-cart-plus"></i> <span> Pesanan </span> 
                            <?php if (mysqli_num_rows($AllOrderSosmedPending) !== 0) { ?><span class="badge badge-warning"><?php echo mysqli_num_rows($AllOrderSosmedPending); ?></span> <?php } ?>
                            <?php if (mysqli_num_rows($AllOrderSosmedProcessing) !== 0) { ?><span class="badge badge-info"><?php echo mysqli_num_rows($AllOrderSosmedProcessing); ?></span> <?php } ?>
                            <?php if (mysqli_num_rows($AllOrderPulsaPending) !== 0) { ?><span class="badge badge-warning"><?php echo mysqli_num_rows($AllOrderPulsaPending); ?></span> <?php } ?>
                            <?php if (mysqli_num_rows($AllOrderPulsaProcessing) !== 0) { ?><span class="badge badge-info"><?php echo mysqli_num_rows($AllOrderPulsaProcessing); ?></span> <?php } ?>
                            </a>
                        </li>  

                        <li class="has-submenu">
                            <a href="#"><i class="mdi mdi-cart-plus"></i> <span> Layanan </span> </a>
                            <ul class="submenu">
                                <li><a href="<?php echo $config['web']['url'];?>admin-dashboard/layanan-sosmed">Sosial Media</a></li>
                                <li><a href="<?php echo $config['web']['url'];?>admin-dashboard/kategori-layanan">Kategori</a></li>
                                <li><a href="<?php echo $config['web']['url'];?>admin-dashboard/laporan-sosmed">Laporan</a></li>
                            </ul>
                        </li>  

                        <li class="has-submenu">
                            <a href="#"><i class="mdi mdi-file-multiple"></i> <span> Aktivitas </span> </a>
                            <ul class="submenu">
                                <li><a href="<?php echo $config['web']['url'];?>admin-dashboard/aktifitas-pengguna">Pengguna</a></li>
                                <li><a href="<?php echo $config['web']['url'];?>admin-dashboard/penggunaan-saldo">Mutasi Saldo</a></li>
                                <li><a href="<?php echo $config['web']['url'];?>admin-dashboard/transfer-saldo">Transfer Saldo</a></li>
                            </ul>
                        </li>        

                        <li class="has-submenu">
                            <a href="#"><i class="fa fa-list"></i> <span> Lainnya </span> </a>
                            <ul class="submenu">
                                <li><a href="<?php echo $config['web']['url'];?>admin-dashboard/berita">Kelola Berita</a></li>
                                <li><a href="<?php echo $config['web']['url'];?>admin-dashboard/halaman-lain">Halaman Web</a></li>
                                <li><a href="<?php echo $config['web']['url'];?>admin-dashboard/harga-pendaftaran">Harga Pendaftaran</a></li>
                                <li><a href="<?php echo $config['web']['url'];?>admin-dashboard/pengaturan-website">Pengaturan Web</a></li>  
                            </ul>
                        </li>       

                    </ul>
                    <!-- End navigation menu  -->

                </div>
            </div>
        </div>
    </header>

    <!-- End Navigation Bar-->

            <?php
            if (isset($_SESSION['hasil'])) {
                ?>
                <div class="alert alert-<?php echo $_SESSION['hasil']['alert'] ?> alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Respon : </strong><?php echo $_SESSION['hasil']['judul'] ?><br /> <strong>Pesan : </strong> <?php echo $_SESSION['hasil']['pesan'] ?>
                </div>
                <?php
                unset($_SESSION['hasil']);
            }
            ?>

            <?php
            $time = microtime();
            $time = explode(' ', $time);
            $time = $time[1] + $time[0];
            $start = $time;
            ?>