<?php
require 'session_login.php';
require 'database.php';
require 'csrf_token.php';
?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="<?php echo $config['web']['meta']['description'] ?>">
	<meta name="keywords" content="<?php echo $config['web']['meta']['keywords'] ?>">
	<meta name="author" content="<?php echo $config['web']['meta']['author'] ?>">
	<title><?php echo $data['short_title']; ?></title>
	<link href="/assets/plugins/morris/morris.css" rel="stylesheet" />
	<link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="/assets/css/icons.css" rel="stylesheet" type="text/css" />
	<link href="/assets/css/style.css" rel="stylesheet" type="text/css" />
	<script src="/assets/js/modernizr.min.js"></script>
	<script src="/assets/js/jquery.min.js"></script>
	<script src="/assets/js/"></script>
	<script src="/assets/js/popper.min.js"></script>
	<script src="/assets/js/bootstrap.min.js"></script>
	<script src="/assets/js/waves.js"></script>
	<script src="/assets/js/jquery.slimscroll.js"></script>
	<script src="/assets/plugins/morris/morris.min.js"></script>
	<script src="/assets/plugins/raphael/raphael-min.js"></script>
	<style type="text/css">
		.hide {
			display: none !important
		}

		.show {
			display: block !important
		}
	</style>
	<script type="text/javascript">
		function modal_open(type, url) {
			$('#modal').modal('show');
			if (type == 'add') {
				$('#modal-title').html('<i class="fa fa-plus-square"></i> Tambah Data');
			} else if (type == 'edit') {
				$('#modal-title').html('<i class="fa fa-edit"></i> Ubah Data');
			} else if (type == 'delete') {
				$('#modal-title').html('<i class="fa fa-trash"></i> Hapus Data');
			} else if (type == 'detail') {
				$('#modal-title').html('<i class="fa fa-search"></i> Detail Data');
			} else {
				$('#modal-title').html('Empty');
			}
			$.ajax({
				type: "GET",
				url: url,
				beforeSend: function() {
					$('#modal-detail-body').html('Sedang memuat...');
				},
				success: function(result) {
					$('#modal-detail-body').html(result);
				},
				error: function() {
					$('#modal-detail-body').html('Terjadi kesalahan.');
				}
			});
			$('#modal-detail').modal();
		}
	</script>
</head>

<body>
	<header id="topnav">
		<div class="topbar-main">
			<div class="container-fluid">
				<div class="logo">
					<a href="<?php echo $config['web']['url']; ?>" class="logo">
						<span class="logo-small"><i class="fa fa-shopping-cart"></i></span>
						<span class="logo-large"><i class="fa fa-shopping-cart"></i> <?php echo $data['short_title']; ?></span>
					</a>
				</div>
				<div class="menu-extras topbar-custom">
					<ul class="list-unstyled topbar-right-menu float-right mb-0">
						<li class="menu-item">
							<a class="navbar-toggle nav-link">
								<div class="lines">
									<span></span>
									<span></span>
									<span></span>
								</div>
							</a>
						</li>
						<?php
						if (isset($_SESSION['user'])) {
						?>
							<li style="padding: 0 10px;">
								<span class="nav-link">Saldo: Rp <?php echo number_format($data_user['saldo'], 0, ',', '.'); ?></span>
							</li>
							<li class="dropdown notification-list">
								<a class="nav-link dropdown-toggle waves-effect nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
									<img src="<?php echo ('assets/images/users/user2.png'); ?>" alt="user" class="rounded-circle"> <span class="ml-1 pro-user-name"><?php echo $sess_username; ?><i class="mdi mdi-chevron-down"></i> </span> </span>
								</a>
								<div class="dropdown-menu dropdown-menu-right profile-dropdown">
									<?php
									if ($data_user['level'] == "Developers") {
									?>
										<a href="/admin-dashboard" class="dropdown-item notify-item"><i class="fa fa-gears"></i> <span>Administrator</span></a>
										<a href="/staff/add_member.php" class="dropdown-item notify-item"><i class="fa fa-gears"></i> <span>Tambah Member</span></a>
										<a href="staff/add_reseller.php" class="dropdown-item notify-item"><i class="fa fa-gears"></i> <span>Transfer Saldo</span></a>
										<a href="staff/add_reseller.php" class="dropdown-item notify-item"><i class="fa fa-gears"></i> <span>Riwayat Transfer</span></a>
									<?php
									}
									?>
									<a href="/user/setting" class="dropdown-item notify-item"><i class="fa fa-gear fa-fw"></i> <span>Pengaturan Akun</span></a>
									<a href="/logout" class="dropdown-item notify-item"><i class="fa fa-sign-out fa-fw"></i> <span>Keluar</span></a>
								</div>
							</li>
						<?php
						}
						?>
					</ul>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="navbar-custom">
			<div class="container-fluid">
				<div id="navigation">
					<ul class="navigation-menu">
						<?php
						if (isset($_SESSION['user'])) {
						?>
							<li>
								<a href="<?php echo $config['web']['url']; ?>"> <i class="fa fa-home"></i> Dashboard</a>
							</li>
							<li>
								<a href="/user/top-member"> <i class="fa fa-trophy"></i> Top 10</a>
							</li>

							<li class="has-submenu">
								<a href="#"><i class="fa fa-shopping-cart"></i> Penambahan</a>
								<ul class="submenu">
									<li><a href="/pemesanan/sosial-media">Tambah</a></li>
									<li><a href="/riwayat/pemesanan-sosmed">Riwayat</a></li>
									<li><a href="/pemesanan/grafik">Grafik</a></li>
								</ul>
							</li>
							<li class="has-submenu">
								<a href="#"><i class="fa fa-money"></i> Deposit</a>
								<ul class="submenu">
									<li><a href="/pay">Isi Saldo</a></li>
									<li><a href="/pay/redeem">Isi Saldo Voucher</a></li>
									<li><a href="/riwayat/deposit-saldo">Riwayat Deposit</a></li>
									<li><a href="/riwayat/deposit-saldo-voucher">Riwayat Deposit Voucher</a></li>
								</ul>
							</li>
							<li>
							<li><a href="/tiket"> <i class="fa fa-envelope"></i> Hubungi Kami</a></li>
							</li>
							<li>
							<li class="has-submenu">
								<a href="#"> <i class="fa fa-book"></i>Monitoring</a>
								<ul class="submenu">
									<li><a href="/status/sosmed">Monitoring Layanan</a></li>
									<li><a href="/halaman/api-dokumentasi-sosmed">Api</a></li>
								</ul>
							</li>
							</li>
							<li>
								<a href="/halaman/daftar-harga"> <i class="fa fa-tags"></i> Daftar Layanan</a>
							</li>

							<li class="has-submenu">
								<a href="#"><i class="fa fa-file"></i> Log</a>
								<ul class="submenu">
									<li><a href="/user/log"> Masuk</a></li>
									<li><a href="/user/pemakaian-saldo"> Penggunaan Saldo</a></li>
								</ul>
							</li>
							<?php
							if ($data_user['level'] != "Member") {
							?>

							<?php
							}
							?>

						<?php
						} else {
						?>
							<li>
								<a href="<?php echo $config['web']['base_url'] ?>"> <i class="fa fa-home"></i> Dashboard</a>
							</li>
							<li>
								<a href="/auth/login.php"> <i class="fa fa-sign-in"></i> Masuk</a>
							</li>
							<li>
								<a href="/auth/register"> <i class="fa fa-user-plus"></i> Daftar</a>
							</li>
							<li>
								<a href="/halaman/daftar-harga"> <i class="fa fa-list"></i> Daftar Layanan</a>
							</li>
							<li class="has-submenu">
								<a href="#"><i class="fa fa-file"></i> Halaman</a>
								<ul class="submenu">
									<li><a href="/halaman/hubungi-kami">Kontak</a></li>
									<li><a href="/halaman/panduan">Ketentuan Layanan</a></li>
									<li><a href="<?php echo $config['web']['base_url'] ?>page/faq.php">Pertanyaan Umum</a></li>
								</ul>
							</li>
						<?php
						}
						?>
					</ul>
				</div>
			</div>
		</div>
	</header>

	<div class="wrapper">
		<div class="container-fluid">
			<div class="modal" id="modal" id="modal-detail" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
				<div class="popup-gallery">
					<div class="col-md-12">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<span aria-hidden="true">&times;</span>
					</div>
					<div class="col-md-12">
					</div>
				</div>
			</div>
		</div>
		<div class="content-page">
			<div class="content">
				<div class="container-fluid">
					<br />

					<?php
					if (isset($_SESSION['hasil'])) {
					?>
						<div class="alert alert-<?php echo $_SESSION['hasil']['alert'] ?> alert-dismissible" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<strong>Respon : </strong><?php echo $_SESSION['hasil']['judul'] ?><br /> <strong>Pesan : </strong> <?php echo $_SESSION['hasil']['pesan'] ?>
						</div>
					<?php
						unset($_SESSION['hasil']);
					}
					?>

					<?php
					$time = microtime();
					$time = explode(' ', $time);
					$time = $time[1] + $time[0];
					$start = $time;
					?>