</div>
</div>
<footer class="footer">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<?php echo $config['web']['title']; ?> &copy; Copyright 2023 Made with <i class="fa fa-heart text-danger"></i> by <a href="" target="_blank">Gabspedia</a>
			</div>
		</div>
	</div>
</footer>
</div>
</div>

<script src="/assets/js/jquery.core.js"></script>
<script src="/assets/js/jquery.app.js"></script>

</body>

</html>
<script>
	$('#news').modal('show');

	function read_news() {
		$.ajax({
			type: "GET",
			url: "<?php echo $config['web']['url'] ?>ajax/read-news.php"
		});
	}
</script>