<?php

session_start();
require("config.php");

if (!isset($_SESSION['user'])) {
	exit(header("Location: " . $config['web']['url'] . "home/"));
} else {
	$navActive = 'dashboard';

	require("lib/header.php");
	$sess_username = $_SESSION['user']['username'];
?>

	<?php
	include_once 'lib/SEOSecretIDN-meta-homepageonly.php';
	?>

	<div class="row">
		<div class="col-md-12">
			<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
					<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
				</ol>
				<div class="carousel-inner">
					<div class="carousel-item activea">
						<a href="#"><img class="d-block w-100" src="<?php echo ('assets/media/slide/aar.jpg'); ?> " alt="Layanan Terbaik 1"></a>
					</div>
					<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Sebelumnya</span>
					</a>
					<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Selanjutnya</span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 text-center" style="margin: 15px 0;">
			<h3 class="text-uppercase"><i class="fa fa-user-circle-o fa-fw"></i> Informasi Akun</h3>
		</div>
		<div class="col-lg-8">
			<div class="card-box">
				<h4 class="m-t-0 m-b-30 header-title"><i class="fa fa-area-chart"></i> Grafik Pesanan & Deposit 7 Hari Terakhir</h4>
				<div id="line-chart" style="height: 200px;"></div>
			</div>
		</div>
		<div class="col-lg-4 text-center">
			<div class="card-box widget-flat border-custom bg-custom text-white">
				<h3 class="m-b-10">Rp <?php echo number_format($data_user['pemakaian_saldo'], 0, ',', '.'); ?></h3>
				<p class="text-uppercase m-b-5 font-13 font-600"><i class="mdi mdi-cart-outline"></i> Pesanan Saya</p>
			</div>
			<div class="card-box widget-flat border-custom bg-custom text-white">
				<i class="mdi mdi-cash-multiple"></i>
				<h3 class="m-b-10"> <?php echo $jumlah_deposit_user; ?></h3>
				<p class="text-uppercase m-b-5 font-13 font-600">Deposit Saya</p>
			</div>
		</div>
		<div class="col-lg-12">
			<div class="profile-user-box card-box bg-custom">
				<div class="row">
					<div class="col-lg-6">
						<span class="pull-left mr-3"><img src="/assets/images/profile.png" alt="Profile" class="thumb-lg rounded-circle"></span>
						<div class="media-body text-white">
							<h4 class="mt-1 mb-1 font-18"><?php echo $sess_username; ?></h4>
							<p class="text-light mb-0">Sisa Saldo: Rp <?php echo number_format($data_user['saldo'], 0, ',', '.'); ?></p>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="text-right">
							<a class="btn btn-light" href="/user/setting"><i class="fa fa-gear fa-spin fa-fw"></i> Pengaturan Akun</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-lg-12 text-center" style="margin: 15px 0;">
			<h3 class="text-uppercase"><i class="fa fa-bullhorn fa-fw"></i> Informasi Webiste</h3>
		</div>
		<div class="col-lg-8">
			<div class="card-box">
				<h4 class="m-t-0 m-b-30 header-title"><i class="fa fa-info-circle"></i> 5 Informasi Terbaru</h4>
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th style="width: 250px;">TANGGAL/WAKTU</th>
								<th>KONTEN</th>
							</tr>
						</thead>
						<tbody>

							<?php $check_news = $conn->query("SELECT * FROM berita ORDER BY id DESC LIMIT 5"); ?>
							<?php while ($data_news = $check_news->fetch_assoc()) { ?>
								<?php
								if ($data_news['tipe'] == "INFORMASI") $btn = "info";
								if ($data_news['tipe'] == "PERINGATAN") $btn = "warning";
								if ($data_news['tipe'] == "PENTING") $btn = "danger";
								if ($data_news['tipe'] == "LAYANAN") $btn = "success";
								if ($data_news['tipe'] == "PERBAIKAN") $btn = "primary";
								?>
								<tr>
									<td><?php echo tanggal_indo($data_news['date']); ?> , <?php echo $data_news['time']; ?></td>
									<td><?= nl2br($data_news['konten']); ?></td>
								</tr>
								<tr>
									<td colspan="3" align="center">
										<a href="<?php echo $config['web']['url'] ?>news.php">Lihat semua...</a>
									</td>
								</tr>

							<?php
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="card-box">
				<h4 class="m-t-0 m-b-30 header-title"><i class="fa fa-link fa-fw"></i> Sitemap</h4>
				<ul>
					<li><a href="/halaman/hubungi-kami">Kontak</a></li>
					<li><a href="/halaman/panduan">Ketentuan Layanan</a></li>
					<li><a href="<?php echo $config['web']['url'] ?>page/faq">Pertanyaan Umum</a></li>
				</ul>
			</div>
		</div>
	</div>


	</div>
	<!-- End Page -->

	</div>
	<!-- End Content -->

	<!-- Start Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- End Scrolltop -->
	<script type="text/javascript">
		$(function() {
			new Morris.Area({
				element: 'line-chart',
				data: [
					<?php
					$list_tanggal = array();
					for ($i = 6; $i > -1; $i--) {
						$list_tanggal[] = date('Y-m-d', strtotime('-' . $i . ' days'));
					}

					for ($i = 0; $i < count($list_tanggal); $i++) {
						$get_order_sosmed = $conn->query("SELECT * FROM pembelian_sosmed WHERE date ='" . $list_tanggal[$i] . "' and user = '$sess_username'");
						$get_order_pulsa = $conn->query("SELECT * FROM pembelian_pulsa WHERE date = '" . $list_tanggal[$i] . "' ");
						print("{ y: '" . tanggal_indo($list_tanggal[$i]) . "', a: " . mysqli_num_rows($get_order_sosmed) . ", b: " . mysqli_num_rows($get_order_pulsa) . " }, ");
					}
					?>
				],
				xkey: 'y',
				ykeys: ['a', 'b'],
				labels: ['Pesanan Sosial Media'],
				lineColors: ['#525af2'],
				gridLineColor: '#000000',
				pointSize: 0,
				lineWidth: 0,
				resize: true,
				parseTime: false
			});
		});
	</script>


<?php
}
require 'lib/footer.php';
?>