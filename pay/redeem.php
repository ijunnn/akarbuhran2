<?php
session_start();
require("../config.php");
require '../lib/session_user.php';
if (isset($_POST['request'])) {
	require '../lib/session_login.php';

	$post_voucher = $conn->real_escape_string(trim(filter($_POST['voucher'])));

	$cek_voucher = $conn->query("SELECT * FROM voucher WHERE voucher = '$post_voucher'");
	$data_voucher = $cek_voucher->fetch_assoc();
	$cek_voucher_rows = mysqli_num_rows($cek_voucher);

	$post_balance = $data_voucher['saldo'];
	$post_voc = $data_voucher['voucher'];

	if (!$post_voucher) {
		$_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Permintaan Gagal', 'pesan' => 'Silahkan Lengkapi Bidang Berikut :<br/> - Kode Voucher');
	} else if ($cek_voucher_rows == 0) {
		$_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Permintaan Gagal', 'pesan' => 'Kode voucher tidak valid.');
	} else if ($data_voucher['status'] == "sudah di redeem") { 
		$_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Permintaan Gagal', 'pesan' => 'Kode voucher sudah digunakan');

	} else {

		$kode = acak_nomor(3).acak_nomor(3);

		$insert_depo = $conn->query("UPDATE voucher set status = 'sudah di redeem', user = '$sess_username', date = '$date', time = '$time' WHERE voucher = '$post_voucher'");
		$insert_depo = $conn->query("INSERT INTO deposit_voucher VALUES ('','$kode', '$sess_username', 'Redeem Voucher', 'VOUCHER' ,'Voucher Deposit', '-','-','$post_balance', '$post_balance', 'Success', 'Website', '$date', '$time')");
		$insert_depo = $conn->query("INSERT INTO history_saldo VALUES ('', '$sess_username', 'Penambahan Saldo', '$post_balance', 'Penambahan Saldo deposit voucher $post_balance', '$date', '$time')");
		$insert_depo = $conn->query("UPDATE users set saldo = saldo + $post_balance WHERE username = '$sess_username'");

		if ($insert_depo == TRUE) {
			$_SESSION['hasil'] = array('alert' => 'success', 'judul' => 'isi saldo Berhasil', 'pesan' => 'Saldo anda telah ditambah dengan nominal Rp '.$post_balance );
			exit(header("location: ".$config['web']['url']));
		} else {
			$_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Permintaan Gagal', 'pesan' => 'Redeem voucher gagal');
		}
	}
}
require("../lib/header.php");
?>

<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                 <?php
            if (isset($_SESSION['hasil'])) {
                ?>
        <div class="alert alert-<?php echo $_SESSION['hasil']['alert'] ?>" role="alert">
    <h4 class="alert-heading"><strong>Respon : </strong><?php echo $_SESSION['hasil']['judul'] ?><br /> <strong></h4>
        <div class="alert-body">
        Pesan : </strong> <?php echo $_SESSION['hasil']['pesan'] ?>
        </div>
    </div>
    <?php
                unset($_SESSION['hasil']);
            }
            ?>

            <?php
            $time = microtime();
            $time = explode(' ', $time);
            $time = $time[1] + $time[0];
            $start = $time;
            ?>
<!--Title-->
<title>Redeem Voucher Deposit</title>
<meta name="description" content="Platform Layanan Digital All in One, Berkualitas, Cepat & Aman. Menyediakan Produk & Layanan Pemasaran Sosial Media, Payment Point Online Bank, Layanan Pembayaran Elektronik, Optimalisasi Toko Online, Voucher Game dan Produk Digital."/>

<div class="row">
	<div class="col-md-7">
		<div class="card">
			<div class="card-body">
				<h4 class="m-t-0 text-uppercase text-center header-title"><i class="ti-wallet text-primary"></i>Voucher Saldo</h4><hr>
				<form class="form-horizontal" role="form" method="POST">
					<input type="hidden" name="csrf_token" value="<?php echo $config['csrf_token'] ?>">
					<div class="form-group">
						<label class="col-md-12 control-label">Kode Voucher *</label>
						<div class="col-md-12">
							<input type="text" class="form-control" name="voucher" placeholder="VC-030609XXXXXXX" id="voucher">
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-offset-2 col-md-12">
							<button type="submit" class="pull-right btn btn-primary btn-block waves-effect w-md waves-light" name="request"><i class="ti-wallet"></i> Redeem</button>
						</div>
					</div>    
				</form>
			</div>
		</div>
		<div class="card">
			<div class="card-body">
				<h4 class="m-t-0 text-uppercase text-center header-title"><i class="ti-wallet text-primary"></i> Beli Saldo</h4><hr>
				<center>
					<h4><p>Anda bisa membeli saldo deposit di admin <?php echo $data['short_title']; ?> melalui WA dibawah ini</p></h4>
					<p>
						<i class="fa fa-arrow-circle-down fa-2x text-primary"></i>
					</p>
					<div class="btn-group m-t-20">
						<a class="btn btn-success waves-effect w-md waves-light" href="https://api.whatsapp.com/send?phone=6281384248407&text=Saya%20mau%20isi%20saldo <?php echo $data['short_title']; ?>"><i class="mdi mdi-whatsapp" aria-hidden="true"></i> WhatsApp</a>
					</div>
				</center>
			</div>
		</div>
	</div>

	<!-- INFORMASI ORDER -->
	<div class="col-md-5">
		<div class="card">
			<div class="card-body">

				<center><h4 class="m-t-0 text-uppercase header-title"><i class="fa fa-info-circle"></i><b> Informasi Deposit</h4></b>
					<b>Keterangan</b> : Redeem ON 24 jam.<hr>
				</center>

				<!--CARA-->
				<div class="table-responsive">
					<center><i class="fa fa-check-circle"></i><b> Langkah Pembelian Saldo</b></center>
					<ol class="list-p">
						<li>Lakukan pembelian voucher di admin/agen.</li>
						<li>Masukkan kode voucher yang valid (belum digunakan).</li>
						<li>Klik <span class="badge badge-primary"><b>Redeem</b></span></li>
					</ol>
				</div>

				<!--KETENTUAN-->
				<div class="table-responsive">
					<center><i class="fa fa-check-circle"></i><b> Syarat & Ketentuan Redeem</b></center>
					<ol class="list-p">
						<li>Dilarang menggunakan voucher yang sudah di redeem.</li>
						<li>Jumlah deposit langsung ditambahkan ke akun jika kode valid.</li>
						<li>Detail redeem voucher tampil di riwayat deposit.</li>
					</ol>
				</div>

			</div>
		</div>
	</div>
	<!-- INFORMASI ORDER -->

	<?php
	require ("../lib/footer.php");
	?>