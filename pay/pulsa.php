<?php
session_start();
require("../config.php");
require '../lib/session_user.php';
if (isset($_POST['request'])) {
	require '../lib/session_login.php';
	
	$post_provider = $conn->real_escape_string($_POST['provider']);
	$post_pembayaran = $conn->real_escape_string($_POST['pembayaran']);
	$post_jumlah = $conn->real_escape_string(trim(filter($_POST['jumlah'])));
	$post_pengirim = $conn->real_escape_string(trim(filter($_POST['pengirim'])));
	
	$cek_metod = $conn->query("SELECT * FROM metode_depo WHERE id = '$post_provider'");
	$data_metod = $cek_metod->fetch_assoc();
	$cek_metod_rows = mysqli_num_rows($cek_metod);
	
	$cek_depo = $conn->query("SELECT * FROM deposit WHERE username = '$sess_username' AND status = 'Pending'");
	$data_depo = $cek_depo->fetch_assoc();
	$count_depo = mysqli_num_rows($cek_depo);
	
	$kode = acak_nomor(3).acak_nomor(3);
	//$acakin = acak_nomor(2).acak_nomor(1);

	if (!$post_provider || !$post_pembayaran || !$post_jumlah) {
		$_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Permintaan Gagal', 'pesan' => 'Lengkapi Bidang Berikut:<br/> - Provider <br /> - Pembayaran <br /> - Nomor Pengirim <br /> - Jumlah');
		
	} else if ($cek_metod_rows == 0) {
		$_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Permintaan Gagal', 'pesan' => 'Metode Deposit Tidak Tersedia.');
		
	} else if ($count_depo >= 1) {
		$_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Permintaan Gagal', 'pesan' => 'Terdapat Deposit Yang Berstatus Pending.');
		exit(header("Location: ".$config['web']['url']."invoice-pulsa"));

	} else if ($post_jumlah < 10000) {
		$_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Permintaan Gagal', 'pesan' => 'Minimal Deposit Pulsa 10000.');
		
	} else if ($post_jumlah > 100000) {
		$_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Permintaan Gagal', 'pesan' => 'Maksimal Deposit Pulsa 100.000.');

	} else {
		
		$metodnya = $data_metod['nama'];
		$get_saldo = $post_jumlah * $data_metod['rate'];
		//$amount = $acakin + $get_saldo;
		//$reg = $acakin + $post_jumlah;
		$insert = $conn->query("INSERT INTO deposit VALUES ('','$kode', '$sess_username', '".$data_metod['tipe']."', '".$data_metod['provider']."' ,'$metodnya', '$post_pengirim','".$data_metod['tujuan']."','$post_jumlah', '$get_saldo', 'Pending', 'Website', '$date', '$time')");
		if ($insert == TRUE) {
			exit(header("Location: ".$config['web']['url']."invoice-pulsa"));
			
			
		} else {
			$_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Permintaan Gagal', 'pesan' => 'Error System(Insert To Database).');
		}
	}
}
require("../lib/header.php");
?>

<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                 <?php
            if (isset($_SESSION['hasil'])) {
                ?>
        <div class="alert alert-<?php echo $_SESSION['hasil']['alert'] ?>" role="alert">
    <h4 class="alert-heading"><strong>Respon : </strong><?php echo $_SESSION['hasil']['judul'] ?><br /> <strong></h4>
        <div class="alert-body">
        Pesan : </strong> <?php echo $_SESSION['hasil']['pesan'] ?>
        </div>
    </div>
    <?php
                unset($_SESSION['hasil']);
            }
            ?>

            <?php
            $time = microtime();
            $time = explode(' ', $time);
            $time = $time[1] + $time[0];
            $start = $time;
            ?>
<!--Title-->
<title>Deposit via Pulsa</title>
<meta name="description" content="Platform Layanan Digital All in One, Berkualitas, Cepat & Aman. Menyediakan Produk & Layanan Pemasaran Sosial Media, Payment Point Online Bank, Layanan Pembayaran Elektronik, Optimalisasi Toko Online, Voucher Game dan Produk Digital."/>

<div class="row">
	<div class="col-md-7">
		<div class="card">
			<div class="card-body">
				<h4 class="m-t-0 text-uppercase text-center header-title"><i class="ti-wallet text-primary"></i> DEPOSIT PULSA</h4><hr>
				<form class="form-horizontal" role="form" method="POST">
					<input type="hidden" name="csrf_token" value="<?php echo $config['csrf_token'] ?>">
					<div class="form-group">
						<label class="col-md-12 control-label">Provider *</label>
						<div class="col-md-12">
							<select class="form-control" name="provider" id="provider">
								<option value="0">Pilih Salah Satu</option>
								<?php
								$cek_kategori = $conn->query("SELECT * FROM metode_depo WHERE provider = 'TSEL' AND keterangan = 'ON' ORDER BY nama ASC");
								while ($data_metode = $cek_kategori->fetch_assoc()) {
									?>
									<option value="<?php echo $data_metode['id'];?>"><?php echo $data_metode['provider'];?></option>
								<?php } ?>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-12 control-label">Pembayaran *</label>
						<div class="col-md-12">
							<select class="form-control" name="pembayaran" id="pembayaran">
								<option value="0">Pilih Provider Pembayaran</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-12 control-label">Nomor Pengirim *<br/>
							<small class="text-danger">Gunakan Awalan 62. Bukan 08 / +62</small>
						</label>
						<div class="col-md-12">
							<input type="text" class="form-control" placeholder="6282377XXXXXX"  name="pengirim">
						</div>
					</div>

					<div class="form-row">
						<div class="form-group col-md-6">
							<label class="col-md-12 col-form-label">Jumlah *</label>
							<div class="col-md-12">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Rp</span>
                                    </div>
                                    <input type="number" class="form-control" name="jumlah" placeholder="Jumlah Deposit" id="jumlah">
                                </div>
                            </div>
						</div>

						<div class="form-group col-md-6">
							<label class="col-md-12 col-form-label">Saldo yang didapat</label>
							<div class="col-md-12">
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">Rp</span>
									</div>
									<input type="text" class="form-control"  name="saldo" placeholder="Saldo Yang Didapat" id="rate" readonly>
								</div>
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-md-offset-2 col-md-12">
							<button type="submit" class="pull-right btn btn-primary btn-block waves-effect w-md waves-light" name="request"><i class="ti-wallet"></i> Deposit</button>
						</div>
					</div>    
				</form>
			</div>
		</div>
	</div>
	<!-- end col -->

	<!-- INFORMASI ORDER -->
	<div class="col-md-5">
		<div class="card">
			<div class="card-body">

				<center><h4 class="m-t-0 text-uppercase header-title"><i class="fa fa-info-circle"></i><b> Informasi Deposit</h4></b>
					Online 24 jam setiap hari<hr>
				</center>

				<!--CARA-->
				<div class="table-responsive">
					<center><i class="fa fa-check-circle"></i><b> Cara Melakukan Deposit</b></center>
					<ol class="list-p">
						<li>Pilih salah satu provider & pembayaran.</li>
						<li>Masukkan nomor pengirim sesuai format.</li>
						<li>Masukkan jumlah deposit.</li>
						<li>Klik <span class="badge badge-primary"><b>Deposit</b></span></li>
					</ol>
				</div>

				<!--KETENTUAN-->
				<div class="table-responsive">
					<center><i class="fa fa-check-circle"></i><b> Syarat & Ketentuan Deposit</b></center>
					<ol class="list-p">
						<li>Minimal deposit Rp.10.000.</li>
						<li>Saldo yang didapat mengikuti rate deposit.</li>
						<li>Detail faktur tampil setelah klik deposit.</li>
					</ol>
				</div>

			</div>
		</div>
	</div>
	<!-- INFORMASI ORDER -->

</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$("#tipe").change(function() {
			var tipe = $("#tipe").val();
			$.ajax({
				url: '<?php echo $config['web']['url'];?>ajax/provider-deposit.php',
				data: 'tipe=' + tipe,
				type: 'POST',
				dataType: 'html',
				success: function(msg) {
					$("#provider").html(msg);
				}
			});
		});
		$("#provider").change(function() {
			var provider = $("#provider").val();
			$.ajax({
				url: '<?php echo $config['web']['url'];?>ajax/pembayaran-deposit.php',
				data: 'provider=' + provider,
				type: 'POST',
				dataType: 'html',
				success: function(msg) {
					$("#pembayaran").html(msg);
				}
			});
		});
		$("#jumlah").change(function(){
			var pembayaran = $("#pembayaran").val();
			var jumlah = $("#jumlah").val();
			$.ajax({
				url : '<?php echo $config['web']['url'];?>ajax/rate-deposit.php',
				type  : 'POST',
				dataType: 'html',
				data  : 'pembayaran='+pembayaran+'&jumlah='+jumlah,
				success : function(result){
					$("#rate").val(result);
				}
			});
		});  
	});

</script>
<?php
require ("../lib/footer.php");
?>