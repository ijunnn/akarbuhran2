<?php
session_start();
require '../config.php';
require '../lib/session_login_admin.php';
require '../lib/header_admin.php';
?>

<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>

        <!-- Page-Title -->
        <div class="row">
            <div class="col-md-12">
                <br />
                <h2 class="text-center">Manajemen Data Akun Provider Server Pusat Database</h2><br />
            </div>
        </div>
        <!-- End-Page-Title -->

        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body table-responsive">
                        <h4 class="m-t-0 header-title text-center">INFORMASI AKUN PUSAT</h4>
                        <hr>

                        <!--MEDANPEDIA-->
                        <hr>
                        <?php
                        $postdata = "api_key=h739eLYCsPoyI6vhSv86zaBzpsl8R6aY&action=profile";
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, "https://boostnaa.com/api/profile");
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        $chresult = curl_exec($ch);
                        //echo $chresult;
                        curl_close($ch);
                        $json_result = json_decode($chresult, true);
                        ?>
                        <h5 class="btn btn-info">BOOSTNAA</h5>
                        <div class="text-dark">
                            Nama: <?php echo $json_result['data']['nama']; ?><br />
                            Username: <?php echo $json_result['data']['username']; ?><br />
                            <b>Saldo: Rp.<?php echo number_format($json_result['data']['saldo'], 0, ',', '.'); ?></b>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card table-responsive">
                    <div class="card-body">
                        <h4 class="m-t-0 header-title text-center">UPDATE LAYANAN & KATEGORI</h4>
                        <hr>
                        <center>
                            <b>Provider BOOSTNAA</b>
                            <div class="center">
                                <a class="btn btn-info waves-effect w-md waves-light" href="../get/upd-all-layanan-sosmed">Upd All Layanan</a>
                            </div><br />
                        </center>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card table-responsive">
                    <div class="card-body">
                        <h4 class="m-t-0 header-title text-center">HAPUS LAYANAN & KATEGORI</h4>
                        <hr>
                        <center>
                            <div class="center">
                                <a class="btn btn-primary waves-effect w-md waves-light" href="../get/del-all-layanan-pulsa-ppob">Dell All Layanan</a>
                            </div> <br />
                            <b> Kategori & Layanan BOOSTNAA</b>
                            <div class="center">
                                <a class="btn btn-info waves-effect w-md waves-light" href="../get/del-all-layanan-sosmed">Dell All Layanan & Kategori</a>
                            </div> <br />
                            <b> Semua BOOSTNAA</b>
                            <div class="center">
                                <a class="btn btn-danger btn-lg waves-effect w-md waves-light" href="../get/del-all-layanan">DELETE ALL</a>
                                <p />
                            </div>
                        </center>
                    </div>

                </div>
            </div>

            <?php
            require '../lib/footer_admin.php';
            ?>