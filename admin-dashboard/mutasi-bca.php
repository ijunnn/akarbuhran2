<?php
session_start();
require '../config.php';
require '../lib/session_login_admin.php';
require '../lib/bca-class.php';

        $data_bca = $conn->query("SELECT * FROM bca WHERE id = 'S1'")->fetch_assoc();

        if (isset($_POST['login'])) {
            $user_id = $conn->real_escape_string(trim(filter($_POST['user_id'])));
            $password = $conn->real_escape_string(trim(filter($_POST['password'])));

            $accept = $conn->query("UPDATE bca SET user_id = '$user_id', password = '$password' WHERE id = 'S1'");
            if ($accept == TRUE) {
               $_SESSION['hasil'] = array('alert' => 'success', 'pesan' => 'Sip, Data Berhasil Di Update.');
            } else {
               $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Gagal! Sistem Kami Sedang Mengalami Gangguan');
            }
        } if (isset($_POST['reset'])) {
            $accept = $conn->query("UPDATE bca SET user_id = '', password = '' WHERE id = 'S1'");
            if ($accept == true) {
               $_SESSION['hasil'] = array('alert' => 'success', 'pesan' => 'Sip, Data Berhasil Direset.');
            } else {
               $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Gagal! Sistem Kami Sedang Mengalami Gangguan');
            }
        }

        require '../lib/header_admin.php';

?>

 <br>
 <br>
  <br>
 <br>
  <br>
 <br>

        <!-- Start Content -->
        <div class="kt-container kt-grid__item kt-grid__item--fluid">

        <!-- Start Page Data Pengaturan Mutation BCA -->
        <div class="row">
	        <div class="offset-lg-2 col-lg-8">
		        <div class="kt-portlet">
			        <div class="kt-portlet__head">
				        <div class="kt-portlet__head-label">
					        <h3 class="kt-portlet__head-title">
					            <i class="fa fa-credit-card text-primary"></i>
					            Pengaturan Akun BCA
					        </h3>
				        </div>
			        </div>
			        <div class="kt-portlet__body">
                    <?php
                    if (isset($_SESSION['hasil'])) {
                    ?>
                    <div class="alert alert-<?php echo $_SESSION['hasil']['alert'] ?> alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $_SESSION['hasil']['pesan'] ?>
                    </div>
                    <?php
                    unset($_SESSION['hasil']);
                    }
                    ?>
                    <form class="form-horizontal" method="POST">
                        <input type="hidden" name="csrf_token" value="<?php echo $config['csrf_token'] ?>">
						<div class="form-group row">
							<label class="col-xl-3 col-lg-3 col-form-label">User ID</label>
							<div class="col-lg-9">
								<input type="text" name="user_id" class="form-control" placeholder="User ID" value="<?= $data_bca['user_id'] ?>">
							</div>
						</div>
						<div class="form-group row">
							<label class="col-xl-3 col-lg-3 col-form-label">Password</label>
							<div class="col-lg-9">
								<input type="text" name="password" class="form-control" placeholder="Password" value="<?= $data_bca['password'] ?>">
							</div>
						</div>
                        <div class="row">
                            <div class="form-group col-6">
                                <button type="submit" name="reset" class="btn btn-danger form-control">Reset</button>
                            </div>
                            <div class="form-group col-6">
                                <button type="submit" name="login" class="btn btn-primary form-control">Login</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
 <div class="row">
    <div class="col-lg-12">
        <div class="card-box">
            <div class="card-body">   
               <h4 class="m-t-0 header-title"><b><i class="fa fa-list"></i>    Daftar Mutasi BCA</b></h4>  

                <form method="GET" action="">
                    <div class="row">
                        <div class="form-group col-lg-4">
                            <label>Tampilkan Beberapa</label>
                            <select class="form-control" name="tampil">
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                        </div>                                                          
                        <div class="form-group col-lg-4">
                            <label>Cari Kata Kunci</label>
                            <input type="text" class="form-control" name="search" placeholder="Cari Kata Kunci" value="">
                        </div>
                        <div class="form-group col-lg-4">
                            <label>Submit</label>
                            <button type="submit" class="btn btn-block btn-dark">Filter</button>
                        </div>
                    </div>
                </form>
                    <div class="table-responsive">
                        <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Tipe</th>
                                    <th>Deskripsi</th>
                                    <th>Jumlah</th>
                                </tr>
                            </thead>
                            <tbody>
<?php 
// start paging config
if (isset($_GET['search'])) {
    $search = $conn->real_escape_string(filter($_GET['search']));

    $cek_layanan = "SELECT * FROM mutasi_bank WHERE keterangan LIKE '%$search%' ORDER BY id DESC"; // edit
} else {
    $cek_layanan = "SELECT * FROM mutasi_bank ORDER BY id DESC"; // edit
}
if (isset($_GET['cari'])) {
$cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
$records_per_page = $cari_urut; // edit
} else {
    $records_per_page = 10; // edit
}

$starting_position = 0;
if(isset($_GET["halaman"])) {
    $starting_position = ($conn->real_escape_string(filter($_GET["halaman"]))-1) * $records_per_page;
}
$new_query = $cek_layanan." LIMIT $starting_position, $records_per_page";
$new_query = $conn->query($new_query);
$no = $starting_position+1;
// end paging config
while ($data_layanan = $new_query->fetch_assoc()) {
?>
                                <tr> 
                                    <th><span class="badge badge-dark"><?php echo $no++ ?></span></th>
                                    <td><span class="badge badge-primary"><?php echo $data_layanan['tanggal']; ?></span></td>
                                    <td><span class="badge badge-success"><?php echo $data_layanan['tipe']; ?></span></td>
                                    <td><?php echo $data_layanan['keterangan']; ?></td>
                                    <td><span class="badge badge-warning">Rp <?php echo number_format($data_layanan['jumlah'],0,',','.'); ?></span></td>
                                </tr>  
<?php } ?>
                            </tbody>
                        </table>
                        <br>
<ul class="pagination">
    <?php
// start paging link
    if (isset($_GET['search'])) {
        $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
    } else {
        $cari_urut =  10;
    }  
    if (isset($_GET['search'])) {
        $search = $conn->real_escape_string(filter($_GET['search']));
        $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
    } else {
        $self = $_SERVER['PHP_SELF'];
    }
    $cek_pengguna = $conn->query($cek_pengguna);
    $total_records = mysqli_num_rows($cek_pengguna);
    echo "<li class='disabled page-item'><a class='page-link' href='#'>Total ".$total_records."</a></li>";
    if($total_records > 0) {
        $total_pages = ceil($total_records/$records_per_page);
        $current_page = 1;
        if(isset($_GET["halaman"])) {
            $current_page = $conn->real_escape_string(filter($_GET["halaman"]));
            if ($current_page < 1) {
                $current_page = 1;
            }
        }
        if($current_page > 1) {
            $previous = $current_page-1;
            if (isset($_GET['search'])) {
                $search = $conn->real_escape_string(filter($_GET['search']));
                $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
                echo "<li class='page-item'><a class='page-link' href='".$self."?halaman=1&tampil=".$cari_urut."&search=".$search."'>← First</a></li>";
                echo "<li class='page-item'><a class='page-link' href='".$self."?halaman=".$previous."&tampil=".$cari_urut."&search=".$search."'><i class='fa fa-angle-left'></i> Prev</a></li>";
            } else {
                echo "<li class='page-item'><a class='page-link' href='".$self."?halaman=1'>← First</a></li>";
                echo "<li class='page-item'><a class='page-link' href='".$self."?halaman=".$previous."'><i class='fa fa-angle-left'></i> Prev</a></li>";
            }
        }
// limit page
        $limit_page = $current_page+3;
        $limit_show_link = $total_pages-$limit_page;
        if ($limit_show_link < 0) {
            $limit_show_link2 = $limit_show_link*2;
            $limit_link = $limit_show_link - $limit_show_link2;
            $limit_link = 3 - $limit_link;
        } else {
            $limit_link = 3;
        }
        $limit_page = $current_page+$limit_link;
// end limit page
// start page
        if ($current_page == 1) {
            $start_page = 1;
        } else if ($current_page > 1) {
            if ($current_page < 4) {
                $min_page  = $current_page-1;
            } else {
                $min_page  = 3;
            }
            $start_page = $current_page-$min_page;
        } else {
            $start_page = $current_page;
        }
// end start page
        for($i=$start_page; $i<=$limit_page; $i++) {
            if (isset($_GET['search'])) {
                $search = $conn->real_escape_string(filter($_GET['search']));
                $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
                if($i==$current_page) {
                    echo "<li class='active page-item'><a class='page-link' href='#'>".$i."</a></li>";
                } else {
                    echo "<li class='page-item'><a class='page-link' href='".$self."?halaman=".$i."&tampil=".$cari_urut."&search=".$search."'>".$i."</a></li>";
                }
            } else {
                if($i==$current_page) {
                    echo "<li class='active page-item'><a class='page-link' href='#'>".$i."</a></li>";
                } else {
                    echo "<li class='page-item'><a class='page-link' href='".$self."?halaman=".$i."'>".$i."</a></li>";
                }        
            }
        }
        if($current_page!=$total_pages) {
            $next = $current_page+1;
            if (isset($_GET['search'])) {
                $search = $conn->real_escape_string(filter($_GET['search']));
                $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
                echo "<li class='page-item'><a class='page-link' href='".$self."?halaman=".$next."&tampil=".$cari_urut."&search=".$search."'>Next <i class='fa fa-angle-right'></i></a></li>";
                echo "<li class='page-item'><a class='page-link' href='".$self."?halaman=".$total_pages."&tampil=".$cari_urut."&search=".$search."'>Terakhir →</a></li>";
            } else {
                echo "<li class='page-item'><a class='page-link' href='".$self."?halaman=".$next."'>Next <i class='fa fa-angle-right'></i></a></li>";
                echo "<li class='page-item'><a class='page-link' href='".$self."?halaman=".$total_pages."'>Last →</a></li>";
            }
        }
    }
// end paging link
    ?>                  
</ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Page Data Pengaturan Mutation OVO -->

        </div>
        <!-- End Content -->

        <!-- Start Scrolltop -->
		<div id="kt_scrolltop" class="kt-scrolltop">
		    <i class="fa fa-arrow-up"></i>
		</div>
		<!-- End Scrolltop -->

<?php
require '../lib/footer_admin.php';
?>